--drop database [BANKOFTESEBADA]
GO
--Create DATABASE [BANKOFTESEBADA]
GO
USE [BANKOFTESEBADA]
GO
--drop procedure [sp_CobrarYDevolverRecibo]
GO
alter PROCEDURE [dbo].[sp_CobrarYDevolverRecibo](
@NoCuenta NVARCHAR(50),
@importe INT,
@referencia NVARCHAR(200)
)
AS
	BEGIN
		if((Select Saldo From Banca where NoCuenta = @NoCuenta)<@importe)
		begin
			return ''
		end
		UPDATE Banca Set Saldo = Saldo-@importe where NoCuenta = @NoCuenta
		WAITFOR DELAY '00:00:01';
		SELECT CAST(NEWID() AS NVARCHAR(50))
	END
GO
/*exec sp_CobrarYDevolverRecibo '1000000',100,'BB95C8B7-3F99-4ABB-8567-87EB85E4C4BF'
GO*/

--EXEC [sp_IniciaBanca]
--drop procedure [sp_IniciaBanca]
--GO
alter PROCEDURE [dbo].[sp_IniciaBanca]
AS
	BEGIN
		DELETE FROM Banca
	END 
GO

--Drop Procedure [sp_RegistrarCliente]
GO
alter PROCEDURE [dbo].[sp_RegistrarCliente] (
	 @Nombre NVARCHAR(50)
	,@nocuenta NVARCHAR(50)
	,@saldoInicial INT
)
AS
	BEGIN
		INSERT into Banca (NoCuenta,Nombre,Saldo) Values (@nocuenta,@Nombre,@saldoInicial)
	END 
GO

--exec [sp_IniciaBanca]








 