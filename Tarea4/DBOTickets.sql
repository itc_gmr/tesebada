--drop database [TICKETTESEBADA]
GO
--Create DATABASE [TICKETTESEBADA]
GO
USE [TICKETTESEBADA]
GO

--drop procedure sp_ReservarLugarYDevolverTicketONada 
GO
Alter PROCEDURE sp_ReservarLugarYDevolverTicketONada (
	@nombre NVARCHAR(50)
)AS
	BEGIN tran
		Declare @reservacion NVARCHAR(50)
		Declare @NumTicket int
		Declare @NuevoAsig int
		if((Select count(*) from ticket where Asignado = 0) = 0)
			begin 
				rollback tran
				return ''
			end

	
		Update top (1) Ticket with (rowlock,readPast) set Asignado = 1, @NumTicket = numticket where Asignado = 0
		Update top (1) Reservacion with (rowlock,readPast) set nombre = @nombre,NumTicket=@NumTicket, @reservacion = reservacion
		where nombre is null
		if @@error <>0
			Begin
				raiserror('Error.',14,15);
				Return ''
				Rollback tran;
			End
		SELECT @reservacion
	commit tran
GO

--drop procedure sp_ConfirmarLugarYRegresarReservacion
GO
Alter PROCEDURE sp_ConfirmarLugarYRegresarReservacion(
	@recibo NVARCHAR(50),
    @reservacion NVARCHAR(50),
    @nombre NVARCHAR(50)
) AS
	BEGIN tran
		Update Ticket with (rowlock,readPast) set Recibo = @recibo where NumTicket =(SELECT NumTicket FROM Reservacion Where Reservacion = @reservacion)
		SELECT 'OK' 
	commit tran
GO

--drop procedure sp_DeshacerReservacion
GO
Alter PROCEDURE sp_DeshacerReservacion(
	@reservacion NVARCHAR(50)
) AS
	BEGIN
		if not exists(SELECT * FROM Reservacion Where Reservacion = @reservacion)
				Begin
					return ''
				End
		Update ticket with (rowlock,readPast) set Asignado = 0 where NumTicket = (SELECT NumTicket From Reservacion where Reservacion = @reservacion)
		update Reservacion set nombre = null where Reservacion= @reservacion
		SELECT 'Nothing ... ' 
	END
GO

--drop procedure [dbo].[sp_IniciaConTickets]
--exec sp_IniciaConTickets 10
GO
Alter PROCEDURE [dbo].[sp_IniciaConTickets] (
	@numerodeTickets INT
	)
	AS
	BEGIN
		DELETE FROM RESERVACION
		DELETE FROM Ticket
		DBCC CHECKIDENT ('[Ticket]', RESEED, 0);
		DBCC CHECKIDENT ('[RESERVACION]', RESEED, 0);
		while @numerodeTickets>0
			BEGIN
				Insert into Ticket(Fecha, Asignado)Values(GETDATE(),0)
				insert into Reservacion (Nombre,Reservacion) Values (null,CAST(NEWID() AS NVARCHAR(50)))
				if((SELECT MAX(NumTicket) FROM Ticket)>=@numerodeTickets)
					break;
				else
					continue
			END
	END
GO