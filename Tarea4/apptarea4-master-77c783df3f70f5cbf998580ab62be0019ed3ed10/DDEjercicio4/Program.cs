﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DDEjercicio4
{
    class Program
    {
        static BlockingCollection<Comprador> ColaDeCompradores;
        static BankManager banco;
        static TicketManager taquilla;
        static Configuracion config;
        static volatile int intentos;
        static int maximointentos;

        static void Main(string[] args)
        {
            Task.Run(() => MainAsync(args));
            Console.Read();
        }

        static async Task MainAsync(string[] args)
        {
            Console.WriteLine("Verificando conexiones y datos ... ");
            config = await LeerArchivoDeConfiguracion();
            Console.WriteLine($"Se compraran {config.NumeroDeTickets} x 2 a {config.CostoDeTicket}");
            var compradores = LeerCompradores().Result;

            Console.WriteLine($"Se leyeron {compradores.Count} compradores " );

            Console.WriteLine("Procesando ... ");
            banco = new BankManager(config.BancoDBConn);
            taquilla = new TicketManager(config.TaquillaDBConn);

            //Inicializa Tickets 
            if (( args != null && args.FirstOrDefault(o => o.ToLower() == "init") != null))
            {
                Console.WriteLine($"Iniciando Taquilla con {config.NumeroDeTickets} Tickets ");
                taquilla.IniciarConTickets(config.NumeroDeTickets);
                Console.WriteLine("Taquilla inicializada");
                Console.WriteLine($"Iniciando banca con {compradores.Count} compradores ");
                banco.InicializarBanca(compradores);
                Console.WriteLine("Banca iniciada ");
            }
            else
            {
                Console.WriteLine("BD No inicializada para inicializarla llamar el comando con el parametro `init` " );
            }

            maximointentos = config.NumeroIntentos * compradores.Count;
            ColaDeCompradores = new BlockingCollection<Comprador>();

            foreach (var c in compradores)
            {
                ColaDeCompradores.Add(c);
                intentos++;
            }

            var tasks = new List<Task>();
            for(int i =0; i < config.Concurrencia; i++)
            {
  
                var x = i;
                var t = Task.Run(async() => await ConsumeLoop($"T{x}"));
                tasks.Add(t);
            }
           var showStopper =  Task.Run(async () =>
           {
               await Task.Delay(config.waitTime); // 30 segundo despues de eso dejar de agregar 
               ColaDeCompradores.CompleteAdding();
               Console.WriteLine($"No se permitira agregar mas consumidores {config.waitTime}ms ha pasado" );
           });

            tasks.Add(showStopper);
            await Task.WhenAll(tasks);
          

            Console.WriteLine("Resultados son ... ");
            foreach(var c in compradores)
            {
                Console.WriteLine($"{c.Nombre}, {c.Ticket} , {c.Error} ");
            }

            Console.Read();
        }

        private static async Task<List<Comprador>> LeerCompradores()
        {
            string[] allLines;
            using (var reader = File.OpenText("Compradores.csv"))
            {
                var allText = await reader.ReadToEndAsync();
                allLines = allText.Split(new[] { "\n" }, StringSplitOptions.None);
            }
            return allLines.Where(l => !string.IsNullOrWhiteSpace(l)).Select(l =>
           {
               var data = l.Split(',');
               return new Comprador()
               {
                   Nombre = data[0],
                   NoCuenta = data[1],
                   SaldoInicial = int.Parse(data[2])
               };
           }).ToList();
        }

        private static async Task<Configuracion> LeerArchivoDeConfiguracion()
        {
            var config = new Configuracion();
            using (var reader = File.OpenText("Config.txt"))
            {
                var fileText = await reader.ReadToEndAsync();
                var lines = fileText.Split(new[] { "\n" }, StringSplitOptions.None);
                var real = lines.Where(l => !string.IsNullOrWhiteSpace(l)
                    && !l.StartsWith("''")
                ).Select(l => l);
                foreach(var line in real)
                {
                    if (line.StartsWith("BancoDBConn="))
                    {
                        config.BancoDBConn = line.Substring("BancoDBConn=".Length);
                    }else if (line.StartsWith("TaquillaDBConn="))
                    {
                        config.TaquillaDBConn = line.Substring("TaquillaDBConn=".Length);
                    }else if (line.StartsWith("Concurrencia="))
                    {
                        config.Concurrencia = int.Parse(line.Substring("Concurrencia=".Length));
                    }
                    else if (line.StartsWith("NumeroDeTickets="))
                    {
                        config.NumeroDeTickets = int.Parse(line.Substring("NumeroDeTickets=".Length));
                    }
                    else if (line.StartsWith("CostoDeTicket="))
                    {
                        config.CostoDeTicket = int.Parse(line.Substring("CostoDeTicket=".Length));
                    }else if (line.StartsWith("Intentos="))
                    {
                        config.NumeroIntentos = int.Parse(line.Substring("Intentos=".Length));
                    }else if (line.StartsWith("TiempoEjecucionMS="))
                    {
                        config.waitTime = int.Parse(line.Substring("TiempoEjecucionMS=".Length));
                    }
                }
            }
            return config;
        }

        private static async Task ConsumeLoop(string threadname)
        {
            string reservacion = string.Empty;
            Comprador c = null;
            while (!ColaDeCompradores.IsCompleted)
            {
                try
                {
                    c = ColaDeCompradores.Take();
                }catch(InvalidOperationException ex)
                {
                    Console.WriteLine($"Abortando ... {threadname} " );
                   
                    break;
                }
                if (c != null)
                {
                    try
                    {
                        Console.WriteLine($"{threadname}Procesando {c.Nombre}  ..." );
                        reservacion = await taquilla.ReservarLugarAsync(c);
                        if (string.IsNullOrWhiteSpace(reservacion))
                        {
                            c.Ticket = "Sin Tickets .... :(";
                            if (!ColaDeCompradores.IsAddingCompleted)
                            {
                                ColaDeCompradores.Add(c);
                            }
                            intentos++;
                            //return;// bad
                            continue;
                        }

                        var recibo = await banco.CobrarAsync(c, config.CostoDeTicket, reservacion);

                        if (string.IsNullOrWhiteSpace(recibo))
                        {
                            await taquilla.DeshacerReservacionAsync(reservacion);
                            c.Ticket = "No tienes lana ..... :( ";
                            if (!ColaDeCompradores.IsAddingCompleted)
                            {
                                ColaDeCompradores.Add(c);
                            }
                            intentos++;
                            continue;
                        }

                        var ok = await taquilla.ConfirmarLugarAsync(c, reservacion, recibo);

                        if (ok == "OK")
                        {
                            c.Ticket = reservacion;
                        }


                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            await taquilla.DeshacerReservacionAsync(reservacion);
                        }
                        catch (Exception ex2)
                        {
                            Console.WriteLine($"{threadname}Error 2 {ex.Message}");
                        }
                        c.Error = ex.Message.ToString();
                    }
                    Console.WriteLine($"{threadname}Procesado {c.Nombre}");
                    if (intentos < maximointentos)
                    {
                        if (!ColaDeCompradores.IsAddingCompleted)
                        {
                            ColaDeCompradores.Add(c);
                        }
                        intentos++;
                    }
                }
            }
            Console.WriteLine($"{threadname} terminando");
        }

        
    }

    public class BankManager
    {
        public string Connection { get; set; }
        public BankManager(string conn)
        {
            Connection = conn;
        }

        internal async Task<string> CobrarAsync(Comprador c, int costoDeTicket, string reservacion)
        {
            var cmd = new SqlCommand("sp_CobrarYDevolverRecibo");
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@NoCuenta", c.NoCuenta);
            cmd.Parameters.AddWithValue("@importe", costoDeTicket);
            cmd.Parameters.AddWithValue("@referencia", reservacion);

            using (var con = new SqlConnection(Connection))
            {

                cmd.Connection = con;
                await cmd.Connection.OpenAsync();

                using (var reader = await cmd.ExecuteReaderAsync())
                {
                    if (reader.Read())
                    {
                        return reader.GetString(0);
                    }
                }

            }

            return string.Empty;
        }

        internal void InicializarBanca(List<Comprador> compradores)
        {
            var cmd = new SqlCommand("sp_IniciaBanca");
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            List<SqlCommand> ccmds = new List<SqlCommand>();


            foreach (var c in compradores)
            {
                ccmds.Add(RegistroCmd(c));
            }

            using (var con = new SqlConnection(Connection))
            {

                cmd.Connection = con;
                cmd.Connection.Open();

                cmd.ExecuteNonQuery();


                foreach (var c in ccmds)
                {
                    c.Connection = con;
                    c.ExecuteNonQuery();
                }
            }

            return;
        }

        private SqlCommand RegistroCmd(Comprador c)
        {
            var cmd = new SqlCommand("sp_RegistrarCliente");
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@nombre", c.Nombre);
            cmd.Parameters.AddWithValue("@nocuenta", c.NoCuenta);
            cmd.Parameters.AddWithValue("@saldoinicial", c.SaldoInicial);

            return cmd;
        }
    }

    public class TicketManager
    {
        public string Connection { get; set; }
        public TicketManager(string conn)
        {
            Connection = conn;
        }

        internal async Task<string> ReservarLugarAsync(Comprador c)
        {
            var cmd = new SqlCommand("sp_ReservarLugarYDevolverTicketONada");
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@nombre", c.Nombre);
            using (var con = new SqlConnection(Connection))
            {

                cmd.Connection = con;
                await cmd.Connection.OpenAsync();

                using(var reader = await cmd.ExecuteReaderAsync())
                {
                    if (reader.Read())
                    {
                        return reader.GetString(0);
                    }
                }

            }

            return string.Empty;
        }

        internal async Task<string> DeshacerReservacionAsync(string reservacion)
        {
            var cmd = new SqlCommand("sp_DeshacerReservacion");
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@reservacion", reservacion);
            using (var con = new SqlConnection(Connection))
            {

                cmd.Connection = con;
                await cmd.Connection.OpenAsync();

                using (var reader = await cmd.ExecuteReaderAsync())
                {
                    if (reader.Read())
                    {
                        return reader.GetString(0);
                    }
                }

            }

            return string.Empty;
        }

        internal async Task<string> ConfirmarLugarAsync(Comprador c, string reservacion, string recibo)
        {
            var cmd = new SqlCommand("sp_ConfirmarLugarYRegresarReservacion");
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@recibo", recibo);
            cmd.Parameters.AddWithValue("@reservacion", reservacion);
            cmd.Parameters.AddWithValue("@nombre", c.Nombre);

            using (var con = new SqlConnection(Connection))
            {

                cmd.Connection = con;
                await cmd.Connection.OpenAsync();

                using (var reader = await cmd.ExecuteReaderAsync())
                {
                    if (reader.Read())
                    {
                        return reader.GetString(0);
                    }
                }

            }

            return string.Empty;
            
        }

        internal void IniciarConTickets(int numeroDeTickets)
        {
            var cmd = new SqlCommand("sp_IniciaConTickets");
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@numerodeTickets", numeroDeTickets);
          

            using (var con = new SqlConnection(Connection))
            {

                cmd.Connection = con;
                cmd.Connection.Open();

                cmd.ExecuteNonQuery();

            }

            return ;
        }
    }

    public class Comprador
    {
        public string Nombre { get; set; }
        public string NoCuenta { get; set; }
        public string Ticket { get; set; } = "Sin Ticket";
        public int SaldoInicial { get; set; }

        public void Procesar()
        {
            Console.WriteLine($"{Nombre} Procesado" );
        }
        public string Error { get; set; }
        public int Intentos { get; private set; } = 0;
    }

    public class Configuracion
    {
        internal int waitTime;

        public string BancoDBConn { get; set; }
        public string TaquillaDBConn { get; set; }
        public int Concurrencia { get; set; }
        public int NumeroDeTickets { get; set;}
        public int CostoDeTicket { get; set; }
        public int NumeroIntentos { get; internal set; }
    }

    
}
