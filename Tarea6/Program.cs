﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using StackExchange.Redis;
using MongoDB.Bson.Serialization;
using ServiceStack.Redis;
using ServiceStack;

namespace Tarea6
{
    class Program
    {
        MongoClient mongo;
        IMongoDatabase database;
        IMongoCollection<Cliente> collec;
        RedisClient redis;
        Random r = new Random();
        const int usuarios = 1000;
        const int iteraciones = 100000;
        static int hits;
        static int miss;
        public Program()
        {
            ModelConfig<Cliente>.Id(d => d._id);
            mongo = new MongoClient("mongodb://CCDS-18:27017");
            mongo.DropDatabase("clientes");
            database = mongo.GetDatabase("clientes");
            collec = database.GetCollection<Cliente>("cliente");
            redis = new RedisClient("localhost",6379);
            hits = miss = 0;
            var lista = GenerarClientes(usuarios);
            InsertarClientes(lista);
            LeerClientesNoCache(lista, iteraciones);
            var valor = redis.As<Cliente>();
            valor.StoreAll(lista as List<Cliente>);
            LeerClientesCache(lista, iteraciones);
            Console.ReadLine();

        }
        public List<Cliente> GenerarClientes(int n)
        {
            var num = 0;
            var lista = new List<Cliente>();
            for (int i = 0; i < n; i++)
            {
                num = r.Next(99999);
                lista.Add(new Cliente() { _id = $"C_{num}_{i}",
                    nombre = $"Usuario{i}",
                    DOB = DateTime.UtcNow,
                    grupo = $"Grupo{num}",
                    domicilio = new Direccion() {
                        calle = $"Calle #{num}",
                        CP = $"{r.Next(99999)}",
                        estado = "Sinaloa",
                        numero = $"{r.Next(9999)}" } });
            }
            return lista;
        }
        public void InsertarClientes(List<Cliente> lista)
        {
            var tiempo = new List<TimeSpan>();
            foreach (var c in lista)
            {
                tiempo.Add(InsertarUnCliente(c));
            }
            Console.WriteLine($"Tardo un promedio de: {tiempo.Average(t => t.Milliseconds)} ms , con un tiempo total de: {tiempo.Max(t => t.TotalMilliseconds)} ms");
        }
        public TimeSpan InsertarUnCliente(Cliente c)
        {
            var inicio = DateTime.UtcNow;
            //Proceso de insertado.
            /*var doc = new BsonDocument
            {
                {"_id", c.idCliente},
                {"nombre", c.nombre},
                {"DOB", c.DOB },
                {"grupo", c.grupo },
                {"direccion", new BsonDocument {
                        { "calle", c.domicilio.calle },
                        { "numero", c.domicilio.numero },
                        { "CP",c.domicilio.CP },
                        { "estado",c.domicilio.estado }
                    }
                }
            };*/
            collec.InsertOneAsync(c);
            return DateTime.UtcNow - inicio;
        }

        public void LeerClientesNoCache(List<Cliente> lista, int n)
        {
            var tiempo = new List<TimeSpan>();
            for (int i = 0; i < n; i++)
            {
                var indice = r.Next(0, lista.Count() - 1);
                var key = lista[indice];
                var clienteLeido = LeerNoCache(key._id);
                tiempo.Add(clienteLeido.Item2);
            }
            Console.WriteLine($"Tardo un promedio de: {tiempo.Average(t => t.Milliseconds)} ms , con un tiempo total de: {tiempo.Max(t => t.TotalMilliseconds)} ms");
        }

        public Tuple<Cliente, TimeSpan> LeerNoCache(string idCliente)
        {
            var inicio = DateTime.UtcNow;
            //Leer el cliente del almacen.
            //Console.WriteLine(idCliente);

            var filtro = Builders<Cliente>.Filter.Eq("_id", idCliente);
            var c = collec.Find(filtro).ToList().ElementAt(0);
            //Console.WriteLine(c.nombre);
            return new Tuple<Cliente, TimeSpan>(c, DateTime.UtcNow - inicio);
        }

        public void LeerClientesCache(List<Cliente> lista, int n)
        {
            var tiempo = new List<TimeSpan>();
            for (int i = 0; i < n; i++)
            {
                var indice = r.Next(0, lista.Count() - 1);
                var key = lista[indice];
                var clienteLeido = LeerCache(key._id);
                tiempo.Add(clienteLeido.Item2);
            }
            Console.WriteLine($"Tardo un promedio de: {tiempo.Average(t => t.Milliseconds)} ms , con un tiempo total de: {tiempo.Max(t => t.TotalMilliseconds)} ms");
        }

        public Tuple<Cliente, TimeSpan> LeerCache(string idCliente)
        {
            var inicio = DateTime.UtcNow;
            
            var cliente = getFromCache(idCliente);
            if (cliente == null)
            {
                cliente = LeerNoCache(idCliente).Item1;
                if (cliente != null)
                    setOnCache(idCliente, cliente);
                //miss
                miss++;
            }
            else
            {
                hits++;
            }
            return new Tuple<Cliente, TimeSpan>(new Cliente(), DateTime.UtcNow - inicio);
        }

        public Cliente getFromCache(string idCliente)
        {
            var redisValor = redis.As<Cliente>();
            var cliente = redisValor.GetById(idCliente);
            return cliente;
        }

        public void setOnCache(string id, Cliente cliente)
        {
            var valor = redis.As<Cliente>();
            var Insercion = valor.Store(cliente);
        }
        static void Main(string[] args)
        {
            new Program();
        }
    }
    public class Cliente
    {
        public string _id { get; set; }
        public string nombre { get; set; }
        public DateTime DOB { get; set; }
        public string grupo { get; set; }
        public Direccion domicilio { get; set; }
    }
    public class Direccion
    {
        public string calle { get; set; }
        public string numero { get; set; }
        public string CP { get; set; }
        public string estado { get; set; }
    }
}
