﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tarea_6
{
    class vehiculo
    {
        public string id { get; set; }
        public string modelo { get; set; }
        public string nombreMarca { get; set; }
        public int anio { get; set; }
        public List<recomendaciones> reco { get; set; } = new List<recomendaciones>();
    }
}
