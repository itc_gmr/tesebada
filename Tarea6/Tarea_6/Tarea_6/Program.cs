﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Windows;
using System.Xml.Linq;

//Guillermo Marquez Ruiz - Bryan David Rocha Serrano
namespace Tarea_6
{
    class Program
    {
        public static void LeerXML()
        {
            XDocument doc = XDocument.Load("vehiculo.xml");

            var recomendaciones = doc.Descendants("recomendaciones").Select(d => new recomendaciones
            {
                usuario = d.Element("usuario").Value,
                descripcion = d.Element("descripcion").Value
            }).ToList();

            List<vehiculo> listaVehiculos = doc.Descendants("vehiculo").Select(
                d => new vehiculo
                {
                    modelo = d.Element("modelo").Value,
                    nombreMarca = d.Element("marca").Value,
                    anio = Convert.ToInt32(d.Element("anio").Value),
                    reco = recomendaciones
                }).ToList();

            foreach (var e in listaVehiculos)
            {
                Console.WriteLine("Escribiendo elemento");
                Console.WriteLine(e.modelo + "   " + e.nombreMarca + "   " + e.anio);
                foreach (var r in e.reco)
                {
                    Console.WriteLine(r.usuario + " " + r.descripcion);
                }
            }
            Console.ReadLine();
        }

        private static XmlNode CrearNodoXmlV(XmlDocument documento, string id_v,string modelo, string marca, string anio, List<recomendaciones> r)
        {
            //Creamos el nodo que deseamos insertar.
            XmlElement vehiculo = documento.CreateElement("vehiculo");

            XmlAttribute id = documento.CreateAttribute("id");
            id.InnerText = id_v;
            vehiculo.SetAttribute("id", id_v);

            XmlElement mod = documento.CreateElement("modelo");
            mod.InnerText = modelo;
            vehiculo.AppendChild(mod);

      
            XmlElement mar = documento.CreateElement("marca");
            mar.InnerText = marca;
            vehiculo.AppendChild(mar);

           
            XmlElement a = documento.CreateElement("anio");
            a.InnerText = anio;
            vehiculo.AppendChild(a);

            XmlElement reco = documento.CreateElement("recomendaciones");
            vehiculo.AppendChild(reco);
            foreach (recomendaciones re in r)
            {
                XmlElement usuario = documento.CreateElement("usuario");
                usuario.InnerText = re.usuario;
                reco.AppendChild(usuario);

                XmlElement descripcion = documento.CreateElement("descripcion");
                descripcion.InnerText = re.descripcion;
                reco.AppendChild(descripcion);
            }

            //XmlElement recomendaciones = CrearNodoXmlR(documento, r);
            return vehiculo;
        }

        static void Main(string[] args)
        {
            
            Random rnd = new Random();
            int numDesc;
            int numUS;
            int nC;
            String []descripciones = { "El carro esta bien bonito","El carro esta feo wakala", "Los asientos son muy comodos", "No me gusta que no tenga bluetooth", "Salio defectuoso!!!!"};
            String []usuarios = { "Juan", "Pedro", "Karen", "Antonio", "Jose" };
            String[] modelos = { "Neon", "RIO", "Fiesta", "Spark", "Sentra" };
            String[] nombreMarcas = { "Dodge", "KIA", "Ford", "Chevrolet", "NISSAN" };
            vehiculo[] v = new vehiculo[5];
            //Crear los 5 vehiculos
            for (int i = 0; i < 5; i++)
            {
                
                nC = rnd.Next(2, 10);
                v[i] = new vehiculo();
                v[i].id = "V" + (i+1);
                v[i].modelo = modelos[i];
                v[i].nombreMarca = nombreMarcas[i];
                v[i].anio = rnd.Next(2010, 2018);
                for(int j = 0; j < nC; j++)
                {
                    numUS = rnd.Next(0, usuarios.Length);
                    numDesc = rnd.Next(0, descripciones.Length);
                    recomendaciones dato = new recomendaciones();
                    dato.usuario = usuarios[numUS];
                    dato.descripcion = descripciones[numDesc];
                    v[i].reco.Add(dato);
                }
            }

            XmlDocument doc = new XmlDocument();

            doc.LoadXml("<Lista_vehiculos></Lista_vehiculos>");

            for (int i = 0; i < v.Length; i++)
            {
                XmlNode vehiculo = CrearNodoXmlV(doc, v[i].id,v[i].modelo, v[i].nombreMarca, v[i].anio + "", v[i].reco);
                XmlNode root = doc.DocumentElement;
                root.InsertAfter(vehiculo, root.LastChild);
            }

            //SETTINGS
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            XmlWriter writer = XmlWriter.Create("vehiculo.xml", settings);
            doc.Save(writer);
            writer.Close();

            LeerXML();
            
        }


    }
}
