private void MuestraCFDI(Comprobante cfdi)
{
    // Se verifica que se leyera correctamente el XML
    if (cfdi != null)
    {
        // Se muestran los datos del nodo Comprobante
        dgvComprobante.Rows.Clear();
        foreach (Atributo atributo in cfdi.Atributos)
        {
            dgvComprobante.Rows.Add(atributo.Nombre, atributo.Valor);
        }

        // Se muestran los datos del Emisor
        if (cfdi.Emisor != null)
        {
            tbRfcEmisor.Text = cfdi.Emisor.RFC;
            tbNombreEmisor.Text = cfdi.Emisor.Nombre;
            if (cfdi.Emisor.RegimenFiscal != null)
                tbRegimenEmisor.Text = cfdi.Emisor.RegimenFiscal.Regimen;

            // Se muestran los datos del DomicilioFiscal del Emisor
            if (cfdi.Emisor.DomicilioFiscal != null)
            {
                dgvDomicilioFiscal.Rows.Clear();
                foreach (Atributo atributo in cfdi.Emisor.DomicilioFiscal.Atributos)
                {
                    dgvDomicilioFiscal.Rows.Add(atributo.Nombre, atributo.Valor);
                }
            }

            // Se muestran los datos del ExpedidoEn del Emisor
            if (cfdi.Emisor.ExpedidoEn != null)
            {
                dgvExpedidoEn.Rows.Clear();
                foreach (Atributo atributo in cfdi.Emisor.ExpedidoEn.Atributos)
                {
                    dgvExpedidoEn.Rows.Add(atributo.Nombre, atributo.Valor);
                }
            }
        }

        // Se muestran los datos del Receptor
        if (cfdi.Receptor != null)
        {
            tbRfcReceptor.Text = cfdi.Receptor.RFC;
            tbNombreReceptor.Text = cfdi.Receptor.Nombre;

            // Se muestran los datos del DomicilioFiscal del Emisor
            if (cfdi.Receptor.Domicilio != null)
            {
                dgvDomiclio.Rows.Clear();
                foreach (Atributo atributo in cfdi.Receptor.Domicilio.Atributos)
                {
                    dgvDomiclio.Rows.Add(atributo.Nombre, atributo.Valor);
                }
            }

            // Se muestran los datos del ExpedidoEn del Emisor
            if (cfdi.Emisor.ExpedidoEn != null)
            {
                dgvExpedidoEn.Rows.Clear();
                foreach (Atributo atributo in cfdi.Emisor.ExpedidoEn.Atributos)
                {
                    dgvExpedidoEn.Rows.Add(atributo.Nombre, atributo.Valor);
                }
            }
        }

        // Se muestran los datos del nodo Conceptos
        dgvConceptos.Rows.Clear();
        for (int i = 0; i < cfdi.Conceptos.NumConceptos; i++)
        {
            dgvConceptos.Rows.Add(cfdi.Conceptos[i].NoIdentificacion, cfdi.Conceptos[i].Cantidad, cfdi.Conceptos[i].Descripion, cfdi.Conceptos[i].Unidad, cfdi.Conceptos[i].ValorUnitario, cfdi.Conceptos[i].Importe);
        }

        // Se muestran los datos de los Impuestos
        if (cfdi.Impuestos != null)
        {
            // Traslados
            if (cfdi.Impuestos.Traslados != null)
            {
                dgvTraslados.Rows.Clear();
                foreach (Traslado traslado in cfdi.Impuestos.Traslados.ListaTraslados)
                {
                    dgvTraslados.Rows.Add(traslado.Impuesto, traslado.Tasa, traslado.Importe);
                }
                tbTotalTraslados.Text = cfdi.Impuestos.TotalImpuestosTrasladados;
            }

            // Retenciones
            if (cfdi.Impuestos.Retenciones != null)
            {
                dgvRetenciones.Rows.Clear();
                foreach (Retencion retencion in cfdi.Impuestos.Retenciones.ListaRetenciones)
                {
                    dgvRetenciones.Rows.Add(retencion.Impuesto, retencion.Importe);
                }
                tbTotalRetenciones.Text = cfdi.Impuestos.TotalImpuestosRetenidos;
            }
        }

        // Se muestran los datos de los Complementos
        if (cfdi.Complemento != null)
        {
            TreeNode raizComplemento = MuestraComplementosCFDI(cfdi.Complemento.Complementos);
            tvComplementos.Nodes.Add(raizComplemento);
            tvComplementos.ExpandAll();
        }

        // Se muestra el XML en el Web Browser
        wbXML.Navigate(tbRutaCFDI.Text);
    }
}

private TreeNode MuestraComplementosCFDI(ComplementoComprobante[] complementos)
{
    TreeNode raiz = new TreeNode("Complementos");
    foreach (ComplementoComprobante complemento in complementos)
    {
        AgregaComplemento(complemento, raiz);
    }
    foreach (TreeNode nodo in raiz.Nodes)
    {
        nodo.ForeColor = Color.OrangeRed;
    }
    return raiz;
}

private TreeNode AgregaComplemento(ComplementoComprobante complemento, TreeNode padre)
{
    // Nodo Actual
    TreeNode actual = new TreeNode(complemento.Tag);

    // Se Agregan los atributos
    foreach (Atributo atributo in complemento.Atributos)
    {
        actual.Nodes.Add(atributo.Nombre + ": " + atributo.Valor);
    }
    // Se procesan los subnodos
    foreach (KeyValuePair<string, NodoXML> nodo in complemento.Subnodos)
    {
        actual.Nodes.Add(AgregaComplemento((ComplementoComprobante)nodo.Value, actual));
    }

    if (padre != null)
        padre.Nodes.Add(actual);
    else
        padre = actual;

    // Se devuelve el Nodo
    return padre;
}
