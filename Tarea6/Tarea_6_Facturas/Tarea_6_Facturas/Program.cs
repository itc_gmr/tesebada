﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;


//Guillermo Marquez Ruiz - Bryan David Rocha Serrano
namespace Tarea_6_Facturas
{
    class Program
    {
        public static void XmlPorURL()
        {
            XDocument doc = XDocument.Load("http://itculiacan.edu.mx/feed/");
            var titulos = doc.Descendants("item").Select(d => d.Element("title").Value);
            var links = doc.Descendants("item").Select(d => d.Element("link").Value);
            int i = 0;
            foreach (var n in titulos)
            {
                Console.WriteLine("Titulo:  " + n);
                Console.WriteLine("Link:    " + links.ElementAt(i));
                i++;
            }
            Console.ReadLine();
        }

        public static void XMLFacturas()
        {
            List<String> rutas = new List<String>();
            rutas.Add("SECFD_20170321_121815.xml");
            rutas.Add("V519000207.xml");
            rutas.Add("6e7f6920-d3ad-4079-a3e3-c2a313f99890%201368.xml");
            rutas.Add("aff6ccce-fb20-417c-8d90-47d132bc43c0.xml");
            rutas.Add("CETF800512GF4_Factura_FE5419_20170312.xml");
            rutas.Add("F0000015199.xml");
            rutas.Add("FACTURA-40354.xml");
            foreach (var cadRuta in rutas)
            {
                Console.WriteLine("--------------------------------------------------------");
                XDocument doc = XDocument.Load(cadRuta);
                var emisor = doc.Descendants(doc.Root.Name.Namespace + "Emisor").Select(d => d.Attribute("nombre"));
                Console.WriteLine("Emisor: " + emisor.ElementAt(0));
                var articulos = doc.Descendants(doc.Root.Name.Namespace + "Concepto").Select(d => d.Attribute("descripcion"));
                var importes = doc.Descendants(doc.Root.Name.Namespace + "Concepto").Select(d => d.Attribute("importe"));
                int i = 0;
                var total = 0.0;
                foreach (var arti in articulos)
                {
                    total += (double)importes.ElementAt(i);
                    Console.WriteLine(arti.ToString());
                    Console.WriteLine("Importes: " + importes.ElementAt(i));
                    i++;
                }
                var sello = doc.Descendants(doc.Root.Name.Namespace + "Comprobante").Select(d => d.Attribute("sello"));
                Console.WriteLine(sello.ElementAt(0));
                Console.WriteLine("Importe total: " + total);
                var impuestos = doc.Descendants(doc.Root.Name.Namespace + "Impuestos").Select(d => d.Attribute("totalImpuestosTrasladados"));
                Console.WriteLine("Impuestos: " + impuestos.ElementAt(0));
            }
            Console.ReadLine();
        }

        static void Main(string[] args)
        {
            XmlPorURL();
            XMLFacturas();
        }
    }
}
