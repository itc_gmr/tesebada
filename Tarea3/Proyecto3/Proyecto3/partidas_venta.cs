﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto3
{
    class partidas_venta
    {
        public int id { get; set; }
        public int ventaid { get; set; }
        public DateTime insertado { get; set; }
        public String producto { get; set; }
        public String descripcion { get; set; }
        public Decimal cantidad { get; set; }
        public Decimal precio { get; set; }
        public int id_remoto { get; set; }
        public String db_origen { get; set; }
    }
}
