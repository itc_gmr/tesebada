﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace Proyecto3
{
    class Ejecucion
    {
        private static SqlConnection cnP;
        public static SqlCommand cmdP;
        public static SqlDataReader drP;

        public static SqlConnection cnA;
        public static SqlConnection cnB;
        public static SqlConnection cnC;

        
        public static SqlCommand cmdC;

        
        public static SqlDataReader drC;

        public static String conexionC = "Data Source=.;Initial Catalog=C;Integrated Security=True";

        //Recuperar el identity
        SqlConnection cnid;
        SqlCommand cmdid;
        SqlDataReader drid;
        String identityVentas = "select IDENT_CURRENT('Ventas')";
        String identityPartidasVenta = "select IDENT_CURRENT('PartidasVenta')";
        String identityPartidasVentaID = "select IDENT_CURRENT('Ventas')";

        //Consultas
        String consultaVenta = "SELECT ventaid, emision, cliente, estado, vendedor, importe, observaciones FROM Ventas WHERE id_remoto is NULL";
        //String consultaPartidasVenta = "SELECT id,ventaid,insertado,producto,descripcion,cantidad,precio FROM PartidasVenta WHERE id_remoto is NULL";
        String consultaPartidasVenta = "select pv.ID,v.ventaid, pv.insertado, pv.Producto, pv.Descripcion,pv.Cantidad, pv.Precio " +
            "from ventas v join PartidasVenta pv on v.ventaid = pv.ventaid where v.id_remoto is null and v.ventaid = @ventaid";

        String insertaVenta = "INSERT INTO [DBO].[Ventas] ([emision],[cliente],[estado],[vendedor],[importe],[observaciones],[id_venta_foranea],[db_origen]) OUTPUT (INSERTED.ventaid) values (@emision,@cliente,@estado,@vendedor,@importe,@observaciones,@id_venta_foranea,@db_origen)";
        String insertaPartidaVenta = "INSERT INTO [DBO].[PartidasVenta] ([ventaid],[insertado],[producto],[descripcion],[cantidad],[precio],[id_partida_foranea],[db_origen]) values (@ventaid,@insertado,@producto,@descripcion,@cantidad,@precio,@id_partida_foranea,@db_origen)";

        String insertaIdRemotoVentas = "UPDATE ventas SET id_remoto = @id_remoto WHERE ventaid = @ventaid";
        String insertaIdRemotoPartidaVenta = "UPDATE PartidasVenta SET id_remoto = @id_remoto WHERE id = @id";
        List<ventas> ventas;

        public SqlConnection connect(SqlConnection cn, String conexion)
        {
            try
            {
                cn = new SqlConnection(conexion);
                cn.Open();
                Console.WriteLine("Conexion correcta");
                return cn;
            }
            catch (Exception e) { Console.WriteLine("Conexion incorrecta: " + e.ToString()); }
            return cn;
        }

        public void extraer_datos(SqlConnection cn, SqlCommand cmd, SqlDataReader dr, String conexion, String nombre)
        {
            cn = connect(cn,conexion);
            int n = 0;
            ventas datoV;
            cmd = new SqlCommand(consultaVenta, cn);
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                ventas = new List<ventas>();
                List<partidas_venta> p_v = new List<partidas_venta>();
                while (dr.Read())
                {
                    datoV = new ventas();
                    datoV.Ventaid = dr.GetInt32(0);
                    datoV.Emision = dr.GetDateTime(1);
                    datoV.cliente = dr.GetString(2);
                    datoV.estado = dr.GetString(3);
                    datoV.vendedor = dr.GetString(4);
                    datoV.importe = dr.GetDecimal(5);
                    datoV.obervaciones = dr.GetString(6);
                    datoV.db_origen = nombre;

                    p_v = extraer_datos_partidas(cnP, cmdP, drP, conexion, nombre, datoV.Ventaid);//Vamos a extraer los datos de la partida de una sola venta

                    datoV.p_v = p_v;

                    ventas.Add(datoV);
                }

                foreach (ventas v in ventas)
                {
                    Console.WriteLine("ID de la venta " + v.Ventaid);
                    Console.WriteLine(" --Partidas de venta-- ");
                    foreach (partidas_venta p in v.p_v)
                    {
                        Console.WriteLine("ID de la partida " + p.id);
                    }
                }

                insertarVentas(nombre);
                insertarIdRemoto(cn, conexion, nombre);
                dr.Close();

                insertarPartidasVenta();
                insertarIdRemotoPartida(cn, conexion, nombre);
                Console.ReadLine();
            }
        }

        private void insertarVentas(String nombre)
        {
            cnC = this.connect(cnC, conexionC);
            cmdC = new SqlCommand(insertaVenta, cnC);
            int n;

            foreach(ventas v in ventas)
            {
                cmdC.Parameters.Clear();
                cmdC.Parameters.AddWithValue("emision", v.Emision);
                cmdC.Parameters.AddWithValue("cliente", v.cliente);
                cmdC.Parameters.AddWithValue("estado", v.estado);
                cmdC.Parameters.AddWithValue("vendedor", v.vendedor);
                cmdC.Parameters.AddWithValue("importe", v.importe);
                cmdC.Parameters.AddWithValue("observaciones", v.obervaciones);
                cmdC.Parameters.AddWithValue("id_venta_foranea", v.Ventaid);
                cmdC.Parameters.AddWithValue("db_origen", v.db_origen);
                n = cmdC.ExecuteNonQuery();

                cmdid = new SqlCommand(identityVentas, cnC);
                drid = cmdid.ExecuteReader();
                if (drid.HasRows)
                {
                    while (drid.Read())
                    {
                        v.id_remoto = Convert.ToInt32(drid.GetDecimal(0));
                    }
                }
                drid.Close();
                Console.WriteLine("Se inserto venta con ID local " + v.Ventaid + " con importe total " + v.importe + " y ID remoto " + v.id_remoto);

                
            }
            cnC.Close();

            //drC.Close();
        }

        private void insertarIdRemoto(SqlConnection cn, String conexion, String nombre)
        {
            cnC = connect(cn, conexion);
            cmdC = new SqlCommand(insertaIdRemotoVentas, cnC);
            int n = 0;

            foreach (ventas v in ventas)
            {
                cmdC.Parameters.Clear();
                cmdC.Parameters.AddWithValue("id_remoto", v.id_remoto);
                cmdC.Parameters.AddWithValue("ventaid", v.Ventaid);
                n = cmdC.ExecuteNonQuery();
                Console.WriteLine("Se inserto el ID Remoto " + v.id_remoto);
            }
        }

        private List<partidas_venta> extraer_datos_partidas(SqlConnection cn, SqlCommand cmd, SqlDataReader dr, String conexion, String nombre, int venta)
        {
            cn = connect(cn, conexion);
            int n = 0;
            //partidas_venta datoPV;
            partidas_venta datoP;
            List<partidas_venta> p_v = new List<partidas_venta>();
            cmd = new SqlCommand(consultaPartidasVenta, cn);
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("ventaid", venta);
            dr = cmd.ExecuteReader();
            Console.WriteLine(dr.HasRows);
            if (dr.HasRows)
            {
                p_v = new List<partidas_venta>();
                while (dr.Read())
                {
                    datoP = new partidas_venta();
                    datoP.id = dr.GetInt32(0);
                    datoP.ventaid = dr.GetInt32(1); //Problema corregido insertando las partidas de venta junto con las ventas, tenindo una lista de partidas de venta en la lista de ventas 
                    datoP.insertado = dr.GetDateTime(2);
                    datoP.producto = dr.GetString(3);
                    datoP.descripcion = dr.GetString(4);
                    datoP.cantidad = dr.GetDecimal(5);
                    datoP.precio = dr.GetDecimal(6);
                    datoP.db_origen = nombre;
                    p_v.Add(datoP);
                    //Console.WriteLine("Se extrajo partida de venta " + datoP.id + " de la venta " + datoP.ventaid + " de la fecha " + datoP.insertado.Day + "/" + datoP.insertado.Month + "/" + datoP.insertado.Year);


                }
                
            }
            return p_v;
        }

        private void insertarPartidasVenta()
        {
            cnC = this.connect(cnC, conexionC);
            cmdC = new SqlCommand(insertaPartidaVenta, cnC);
            int n;

            foreach (ventas v in ventas)
            {
                foreach(partidas_venta p in v.p_v)
                {

                    cmdC.Parameters.Clear();
                    cmdC.Parameters.AddWithValue("ventaID", v.id_remoto);
                    cmdC.Parameters.AddWithValue("insertado", p.insertado);
                    cmdC.Parameters.AddWithValue("producto", p.producto);
                    cmdC.Parameters.AddWithValue("descripcion", p.descripcion);
                    cmdC.Parameters.AddWithValue("cantidad", p.cantidad);
                    cmdC.Parameters.AddWithValue("precio", p.precio);
                    cmdC.Parameters.AddWithValue("id_partida_foranea", p.id);
                    cmdC.Parameters.AddWithValue("db_origen", p.db_origen);
                    n = cmdC.ExecuteNonQuery();

                    cmdid = new SqlCommand(identityPartidasVenta, cnC);
                    drid = cmdid.ExecuteReader();
                    if (drid.HasRows)
                    {
                        while (drid.Read())
                        {
                            p.id_remoto = Convert.ToInt32(drid.GetDecimal(0));
                        }
                    }
                    drid.Close();
                    Console.WriteLine("Se inserto partida de venta con ID local " + p.id + " con cantidad total " + p.cantidad);
                }
                
            }
            cnC.Close();
            //drC.Close();
        }

        private void insertarIdRemotoPartida(SqlConnection cn, String conexion, String nombre)
        {
            cnC = connect(cn, conexion);
            cmdC = new SqlCommand(insertaIdRemotoPartidaVenta, cnC);
            int n = 0;

            foreach (ventas v in ventas)
            {
                foreach (partidas_venta p in v.p_v)
                {
                    cmdC.Parameters.Clear();
                    cmdC.Parameters.AddWithValue("id_remoto", p.id_remoto);
                    cmdC.Parameters.AddWithValue("id", p.id);
                    n = cmdC.ExecuteNonQuery();
                    Console.WriteLine("Se inserto el ID Remoto " + p.id_remoto);
                }
            }

        }
    }
}
