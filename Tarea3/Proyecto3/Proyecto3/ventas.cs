﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto3
{
    class ventas
    {
        public int Ventaid { get; set; }
        public DateTime Emision { get; set; }
        public String cliente { get; set; }
        public String estado { get; set; }
        public String vendedor { get; set; }
        public Decimal importe { get; set; }
        public String obervaciones { get; set; }
        public int id_remoto { get; set; }
        public String db_origen { get; set; }
        public List<partidas_venta> p_v { get; set; } = new List<partidas_venta>();
    }
}
