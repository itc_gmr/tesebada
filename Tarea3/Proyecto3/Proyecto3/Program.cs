﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace Proyecto3
{
    class Program
    {
        //Conexiones a bases de datos
        public static SqlConnection cnA;
        public static SqlConnection cnB;

        //Comandos a bases de datos
        public static SqlCommand cmdA;
        public static SqlCommand cmdB;

        //Data reader de bases de datos
        public static SqlDataReader drA;
        public static SqlDataReader drB;

        //Strings de conexion
        public static String conexionA = "Data Source=.;Initial Catalog=A;Integrated Security=True";
        public static String conexionB = "Data Source=.;Initial Catalog=B;Integrated Security=True";


        public static String nombreA = "A";
        public static String nombreB = "B";
        static void Main(string[] args)
        {
            Ejecucion e = new Ejecucion();
            e.extraer_datos(cnA, cmdA, drA, conexionA, nombreA);
            e.extraer_datos(cnB, cmdB, drB, conexionB, nombreB);
        }
    }
}
