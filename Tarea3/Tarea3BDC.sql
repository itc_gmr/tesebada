CREATE TABLE Ventas (
	VentaId INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	Emision DATETIME DEFAULT(GETDATE())NOT NULL,
	Cliente NVARCHAR(50) NOT NULL,
	ESTADO NVARCHAR(20) NOT NULL,
	Vendedor NVARCHAR(20) NOT NULL,
	Importe MONEY NOT NULL,
	Observaciones NVARCHAR(200) NOT NULL
)

--DROP TABLE PartidasVenta
CREATE TABLE PartidasVenta (
	ID INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	VentaId INT NOT NULL,
	Insertado DATETIME DEFAULT(GETDATE()) NOT NULL,
	Producto NVARCHAR(50) NOT NULL,
	Descripcion NVARCHAR(100) NOT NULL,
	Cantidad DECIMAL(10,2) NOT NULL,
	Precio MONEY NOT NULL

)
ALTER TABLE PartidasVenta    
ADD CONSTRAINT FK_Ventas_PartidasVentaVentaId FOREIGN KEY (VentaId)     
    REFERENCES Ventas (VentaId)     
    ON DELETE CASCADE    
    ON UPDATE CASCADE    
;

--Para la base de datos C
ALTER TABLE Ventas
ADD id_venta_foranea int null

ALTER TABLE Ventas
ADD db_origen nvarchar(10)

ALTER TABLE PartidasVenta
ADD id_partida_foranea int null

ALTER TABLE PartidasVenta
ADD db_origen nvarchar(10)

ALTER TABLE Ventas
ADD DEFAULT('C') FOR db_origen

ALTER TABLE PartidasVenta
ADD DEFAULT('C') FOR db_origen

--Consultas
select * from ventas

select * from Partidasventa

delete from ventas

delete from PartidasVenta

select IDENT_CURRENT('Ventas');

IF OBJECT_ID ( 'dbo.uspInsertarVenta', 'P' ) IS NOT NULL   
    DROP PROCEDURE dbo.uspInsertarVenta;  
GO  
CREATE PROCEDURE dbo.uspInsertarVenta  (
  @Emision DATETIME,
  @Cliente NVARCHAR(50),
  @Estado NVARCHAR(20),
  @Vendedor NVARCHAR(20),
  @Importe MONEY,
  @Observaciones NVARCHAR(200)

)
AS  
   INSERT INTO Ventas (Emision, Cliente, Estado, Vendedor, Importe, Observaciones ) 
   OUTPUT INSERTED.VentaId as id 
                 VALUES ( @Emision, @Cliente, @Estado, @Vendedor, @Importe, @Observaciones )  
GO 



IF OBJECT_ID ( 'dbo.uspInsertarPartidaVenta', 'P' ) IS NOT NULL   
    DROP PROCEDURE dbo.uspInsertarPartidaVenta;  
GO  
CREATE PROCEDURE dbo.uspInsertarPartidaVenta  (
  @VentaId INT, 
  @Insertado DATETIME, 
  @Producto NVARCHAR(50), 
  @Descripcion NVARCHAR(200), 
  @Cantidad DECIMAL(10,2), 
  @Precio MONEY
)
AS  
   INSERT INTO PartidasVenta (VentaId, Insertado, Producto, Descripcion, Cantidad, Precio) 
                 VALUES (@VentaId, @Insertado, @Producto, @Descripcion, @Cantidad, @Precio) 
GO