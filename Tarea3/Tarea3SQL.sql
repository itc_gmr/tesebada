CREATE TABLE Ventas (
	VentaId INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	Emision DATETIME DEFAULT(GETDATE())NOT NULL,
	Cliente NVARCHAR(50) NOT NULL,
	ESTADO NVARCHAR(20) NOT NULL,
	Vendedor NVARCHAR(20) NOT NULL,
	Importe MONEY NOT NULL,
	Observaciones NVARCHAR(200) NOT NULL
)

--DROP TABLE PartidasVenta
CREATE TABLE PartidasVenta (
	ID INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	VentaId INT NOT NULL,
	Insertado DATETIME DEFAULT(GETDATE()) NOT NULL,
	Producto NVARCHAR(50) NOT NULL,
	Descripcion NVARCHAR(100) NOT NULL,
	Cantidad DECIMAL(10,2) NOT NULL,
	Precio MONEY NOT NULL

)
ALTER TABLE PartidasVenta    
ADD CONSTRAINT FK_Ventas_PartidasVentaVentaId FOREIGN KEY (VentaId)     
    REFERENCES Ventas (VentaId)     
    ON DELETE CASCADE    
    ON UPDATE CASCADE    
;

--Para las bases de datos A y B
ALTER TABLE Ventas
ADD id_remoto int null

ALTER TABLE PartidasVenta
ADD id_remoto int null

--Consultas
select * from Ventas

select * from PartidasVenta

delete from ventas

delete from PartidasVenta

--v.emision, v.cliente, v.estado, v.vendedor, v.importe, v.Observaciones, 
select pv.ID,v.ventaid, 
pv.insertado, pv.Producto, pv.Descripcion,pv.Cantidad, pv.Precio
from ventas v join PartidasVenta pv on v.ventaid = pv.ventaid where pv.id_remoto is null

select IDENT_CURRENT('Ventas');

SELECT ventaid, emision, cliente, estado, vendedor, importe, observaciones FROM Ventas WHERE id_remoto is NULL
SELECT id,ventaid,insertado,producto,descripcion,cantidad,precio FROM PartidasVenta WHERE id_remoto is NULL
SELECT id,ventaid,insertado,producto,descripcion,cantidad,precio FROM PartidasVenta WHERE VentaId = 243
GO

IF OBJECT_ID ( 'dbo.uspInsertarVenta', 'P' ) IS NOT NULL   
    DROP PROCEDURE dbo.uspInsertarVenta;  
GO  
CREATE PROCEDURE dbo.uspInsertarVenta  (
  @Emision DATETIME,
  @Cliente NVARCHAR(50),
  @Estado NVARCHAR(20),
  @Vendedor NVARCHAR(20),
  @Importe MONEY,
  @Observaciones NVARCHAR(200)

)
AS  
   INSERT INTO Ventas (Emision, Cliente, Estado, Vendedor, Importe, Observaciones ) 
   OUTPUT INSERTED.VentaId as id 
                 VALUES ( @Emision, @Cliente, @Estado, @Vendedor, @Importe, @Observaciones )  
GO 



IF OBJECT_ID ( 'dbo.uspInsertarPartidaVenta', 'P' ) IS NOT NULL   
    DROP PROCEDURE dbo.uspInsertarPartidaVenta;  
GO  
CREATE PROCEDURE dbo.uspInsertarPartidaVenta  (
  @VentaId INT, 
  @Insertado DATETIME, 
  @Producto NVARCHAR(50), 
  @Descripcion NVARCHAR(200), 
  @Cantidad DECIMAL(10,2), 
  @Precio MONEY
)
AS  
   INSERT INTO PartidasVenta (VentaId, Insertado, Producto, Descripcion, Cantidad, Precio) 
                 VALUES (@VentaId, @Insertado, @Producto, @Descripcion, @Cantidad, @Precio) 
GO