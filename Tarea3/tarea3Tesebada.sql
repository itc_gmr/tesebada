
--1.-Descargar la aplicaci�n de Venta APP1 Tarea2.zip
--2.- Crear  3 bases de datos para esta app  A, B, C
CREATE DATABASE A
Use A
CREATE DATABASE B
Use B
CREATE DATABASE C
Use C
--3.- Base de datos C debe de crearse en Azure.
--4.- Inicializarlas con el script DBinit.sql
Use A
DECLARE @Rango Int

Set @Rango = 100
 
DECLARE @VentaId Table(VID Int)

Insert into @VentaId
SELECT Top (@Rango) VentaId
FROM Ventas
Where IdRemoto Is NULL
Order By VentaId ASC


	SELECT V.VentaId,
		V.Vendedor,
		V.Cliente,
		V.Emision,
		V.ESTADO,
		V.Importe,
		V.Observaciones,
		V.idRemoto
	From Ventas V
	JOIN @VentaId VI ON V.VentaId = VI.VID

	SELECT 
		PV.VentaId,
		PV.ID as IDPartida,
		PV.Insertado,
		PV.Producto,
		PV.Descripcion,
		PV.Cantidad,
		PV.Precio
	From PartidasVenta PV
	JOIN @VentaId VI ON PV.VentaId = VI.VID


SELECT * FROM Ventas
SELECT * FROM PartidasVenta

Alter Table Ventas Drop Column idOriginal
Alter Table Ventas Drop Column Origen
ALTER TABLE Ventas ADD idOriginal INT NULL
ALTER TABLE Ventas ADD Origen NVARCHAR(10) NULL
Alter Table Ventas Drop Column IdRemoto
ALTER TABLE Ventas ADD idRemoto INT NULL
UPDATE VENTAS set IdRemoto = null

--5.- Modificar Datos en las sucursales los vendedores deben de ser distintos. 
--6.- Pueden agregar columnas a las tablas si as� lo requieren.
--7.- Pueden modificar los procedimientos almacenados.
--8.- La aplicaci�n debe de funcionar igual, sin errores y pudiendo insertar datos. 
--9.- La funcion Tick esta pensada para subir datos usando un procedimiento almacenado y linked servers (Esta es una solucion). 
--10.- Generar un programa script que mueva registros de las bases de datos A, B hacia C. (Esta es la otra solucion). Las reglas del programa son similares a las de la tarea 2 
--11.- C puede generar sus propios registros. 
--12.- No se deben de duplicar los datos.
--13.- Cuando se ejecute la aplicaci�n en C debo de ver que vendedores de que sucursales est�n vendiendo en cantidad e importe.
