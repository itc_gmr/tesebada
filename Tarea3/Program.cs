﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tarea3
{
    class Program
    {
        private static string ConOrigen = @"Server=CCDS-17\TEW_SQLEXPRESS;Database=A;Trusted_Connection=true";
        private static string ConDestino = @"Server=tcp:cserver.database.windows.net,1433;Initial "+
                            "Catalog=C;Persist Security Info=False;User ID=manuelamd1;Password=Ywahe8xe;"+
                            "MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
        private static string Lectura = "DECLARE @VentaId Table(VID Int) " +
                                "Insert into @VentaId " +
                                "SELECT Top(@Rango) VentaId " +
                                "FROM Ventas " +
                                "Where IdRemoto Is NULL " +
                                "Order By VentaId ASC " +
                                "SELECT V.VentaId, " +
                                "    V.Vendedor, " +
                                "    V.Cliente, " +
                                "    V.Emision, " +
                                "    V.ESTADO, " +
                                "    V.Importe, " +
                                "    V.Observaciones " +
                                "From Ventas V " +
                                "JOIN @VentaId VI ON V.VentaId = VI.VID " +
                                "SELECT "+
                                "PV.VentaId, "+
		                        "    PV.ID as IDPartida, "+
		                        "    PV.Insertado, "+ 
		                        "    PV.Producto, "+
		                        "    PV.Descripcion, "+
		                        "    PV.Cantidad, "+
		                        "    PV.Precio "+
                                "From PartidasVenta PV "+
                                "JOIN @VentaId VI ON PV.VentaId = VI.VID";
        static string A = "A",B = "B";
        Program()
        {
            Console.WriteLine("Empezando a subir datos");
            var Ventas = LeerVentasAsync(10).Result;
            var tuplas = InsertarVentas(Ventas);
            ActualizarValoresLocales(tuplas);
            Console.WriteLine("Se termino de subir las ventas");
            Console.ReadLine();
        }

        private List<Tuple<int, int>> InsertarVentas(List<VentasDTO> Ventas)
        {
            var Tuplas = new List<Tuple<int, int>>();
            foreach (var v in Ventas)
            {
                var id =InsertarVenta(v);
                Tuplas.Add(new Tuple<int, int>(v.VentaID, id));
            }
            return Tuplas;
        }
        private const string UPDATELOCALCMDSTR = "UPDATE Ventas SET idRemoto = @idRemoto WHERE Ventaid=@LocalId";
        private static void ActualizarValoresLocales(List<Tuple<int, int>> tuplas)
        {
            using (var con = new SqlConnection(ConOrigen))
            {
                con.Open();
                foreach (var tp in tuplas)
                {
                    var idLocal = tp.Item1;
                    var idRemote = tp.Item2;
                    var cmd = new SqlCommand(UPDATELOCALCMDSTR);
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@LocalId", idLocal);
                    cmd.Parameters.AddWithValue("@idRemoto", idRemote);
                    cmd.ExecuteNonQuery();
                }
            }
        }
        private static string Insertar =" INSERT INTO VENTAS("+
	                                    "    Emision,"+
	                                    "    Cliente,"+
	                                    "    ESTADO,"+
	                                    "    Vendedor,"+
	                                    "    Importe,"+
	                                    "    Observaciones,"+
                                        "    IdOriginal," +
                                        "    Origen "+
                                        ") OUTPUT (inserted.VentaId) VALUES (" +
                                        "    @Emision,"+
                                        "    @Cliente,"+
                                        "    @ESTADO,"+
                                        "    @Vendedor,"+
                                        "    @Importe,"+
                                        "    @Observaciones,"+
                                        "    @IdOriginal,"+
                                        "    @Origen )";
        private static string InsertarPartidas = "INSERT INTO PartidasVenta("+
                                                 "   VentaId,"+
	                                             "   Insertado,"+
	                                             "   Producto,"+
	                                             "   Descripcion,"+
	                                             "   Cantidad,"+
	                                             "   Precio"+
                                                 " ) VALUES ("+
                                                 "   @VentaId,"+
                                                 "   @Insertado,"+
                                                 "   @Producto,"+
                                                 "   @Descripcion,"+
                                                 "   @Cantidad,"+
                                                 "   @Precio )";
        private int InsertarVenta(VentasDTO V)
        {
            int ID=0;
            var Cmd = new SqlCommand(Insertar);
            Cmd.Parameters.AddWithValue("@Emision", V.Emision);
            Cmd.Parameters.AddWithValue("@Cliente", V.Cliente);
            Cmd.Parameters.AddWithValue("@ESTADO", V.Estado);
            Cmd.Parameters.AddWithValue("@Vendedor", V.Vendedor);
            Cmd.Parameters.AddWithValue("@Importe", V.Importe);
            Cmd.Parameters.AddWithValue("@Observaciones", V.Observaciones);
            Cmd.Parameters.AddWithValue("@IdOriginal", V.VentaID);
            Cmd.Parameters.AddWithValue("@Origen", A);
            try
            {
                using (var Con = new SqlConnection(ConDestino))
                {
                    Cmd.Connection = Con;
                    Cmd.Connection.Open();
                    var Ret = Cmd.ExecuteScalar();
                    if(Ret != null && Ret is int)
                        V.VentaRemota = (int) Ret;
                    foreach (var PV in V.Partidas)
                    {
                        var Cmd2 = new SqlCommand(InsertarPartidas);
                        Cmd2.Parameters.AddWithValue("@VentaId", V.VentaRemota);
                        Cmd2.Parameters.AddWithValue("@Insertado", PV.Insertado);
                        Cmd2.Parameters.AddWithValue("@Producto", PV.Producto);
                        Cmd2.Parameters.AddWithValue("@Descripcion", PV.Descripcion);
                        Cmd2.Parameters.AddWithValue("@Cantidad", PV.Cantidad);
                        Cmd2.Parameters.AddWithValue("@Precio", PV.Precio);
                        Cmd2.Connection = Con;
                        Cmd2.ExecuteNonQuery();
                    }
                    ID = V.VentaRemota;
                }
            } catch(Exception e)
            {
                Console.WriteLine($"No se pudieron Escribir las ventas, error {e.Message}");
            }
            return ID;
        }

        static void Main(string[] args)
        {
           new Program();
        }
        public static async Task<List<VentasDTO>> LeerVentasAsync(int Rango)
        {
            var Cmd = new SqlCommand(Lectura);
            Cmd.Parameters.AddWithValue("@Rango", Rango);
            var Ventas = new Dictionary<int,VentasDTO>();
            try
            {
                using (var Con = new SqlConnection(ConOrigen))
                {
                    Cmd.Connection = Con;
                    await Cmd.Connection.OpenAsync();
                    VentasDTO V = null;
                    using (var Read = await Cmd.ExecuteReaderAsync())
                    {
                        while (await Read.ReadAsync())
                        {
                            V = new VentasDTO();
                            V.VentaID = Read.GetInt32(0);
                            V.Vendedor = Read.GetString(1);
                            V.Cliente = Read.GetString(2);
                            V.Emision = Read.GetDateTime(3);
                            V.Estado = Read.GetString(4);
                            V.Importe = Read.GetDecimal(5);
                            V.Observaciones = Read.GetString(6);
                            if (!Ventas.ContainsKey(V.VentaID))
                                Ventas.Add(V.VentaID, V);
                        }
                        if (await Read.NextResultAsync()) 
                            while (await Read.ReadAsync())
                            {
                                var PV = new PartidasVentasDTO();
                                PV.VentaID = Read.GetInt32(0);
                                PV.ID = Read.GetInt32(1);
                                PV.Insertado = Read.GetDateTime(2);
                                PV.Producto = Read.GetString(3);
                                PV.Descripcion = Read.GetString(4);
                                PV.Cantidad = Read.GetDecimal(5);
                                PV.Precio = Read.GetDecimal(6);
                                if (Ventas.ContainsKey(PV.VentaID))
                                    Ventas[PV.VentaID].Partidas.Add(PV);
                            }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"No se pudieron leer las ventas, error {e.Message}");
            }
            return Ventas.Values.ToList();
        }
        public class VentasDTO
        {
            public int VentaID { get; set; }
            public DateTime Emision { get; set; }
            public string Cliente { get; set; }
            public string Estado { get; set; }
            public string Vendedor { get; set; }
            public decimal Importe { get; set; }
            public string Observaciones { get; set; }
            public List<PartidasVentasDTO> Partidas { get; private set; } = new List<PartidasVentasDTO>();
            public int VentaRemota { get; internal set; }
        }
        public class PartidasVentasDTO
        {
            public int ID { get; set; }
            public int VentaID { get; set; }
            public DateTime Insertado { get; set; }
            public string Producto { get; set; }
            public string Descripcion { get; set; }
            public decimal Cantidad { get; set; }
            public decimal Precio { get; set; }

        } 
    }
}
