﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace ConsoleApp1
{
    class Conexion
    {
        SqlConnection cnLocal;
        SqlConnection cnRemoto;
        SqlCommand cmd;
        SqlDataReader dr;

        SqlCommand cmd2;
        SqlDataReader dr2;
        String identity = "select IDENT_CURRENT('VentasA');";

        SqlCommand cmd3;
        SqlDataReader dr3;

        String conexionLocal = "Data Source=.;Initial Catalog=tesebada;Integrated Security=True";
        String conexionRemota = "Server=tcp:14170932.database.windows.net,1433;Initial Catalog=tesebada;Persist Security Info=False;User ID=Guillermo;Password=Gmr1712.;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

        String consultaLocal = "select id, prod, cantidad, precio, costo, venta, ts, linea, caja, cajero from Ventas where idRemoto is null";
        String insertarRemoto = "INSERT INTO [DBO].[VentasA]" +
            "([Prod],[CANTIDAD],[PRECIO],[COSTO],[VENTA],[TS],[LINEA],[CAJA],[CAJERO],[IDORIGINAL],[ORIGINAL])" +
            "OUTPUT(INSERTED.id) " +
            "values (@Prod, @CANTIDAD, @PRECIO, @COSTO, @VENTA, @TS, @LINEA, @CAJA, @CAJERO, @idoriginal, @Original)";
        String queryInsertarIdRemoto = "UPDATE Ventas SET idRemoto = @idRemoto where id = @id";

        

        String dbOringen = "A";

        //box
        List<ventas_local> vl;
        List<ventas_remoto> vr;
        int xRemoto;
        int xLocal;

        //public SqlParameterCollection Parameters { get; }

        public void connectLocal()
        {
            try
            {
                cnLocal = new SqlConnection(conexionLocal);
                cnLocal.Open();
                Console.WriteLine("Conexion local correcta!!");
                //cn.Close();
                //Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine("No se conecto" + ex.ToString());
                Console.ReadLine();
            }
        }

        public void connectRemoto()
        {
            try
            {
                cnRemoto = new SqlConnection(conexionRemota);
                cnRemoto.Open();
                Console.WriteLine("Conexion remota correcta!!");
                //cn.Close();
                //Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine("No se conecto" + ex.ToString());
                Console.ReadLine();
            }
        }

        public void cerrarConexion(SqlConnection c)
        {
            try
            {
                c.Close();
                Console.WriteLine("Se cerro");
            }
            catch (Exception ex)
            {
                Console.WriteLine("No se conecto" + ex.ToString());
                Console.ReadLine();
            }
        }

        public void extraccion()
        {
            this.connectLocal();
            int n = 0;
            ventas_local dato;
            cmd = new SqlCommand(consultaLocal, cnLocal);
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                vl = new List<ventas_local>();
                while (dr.Read())
                {
                    
                    //Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}", dr.GetInt32(0),dr.GetString(1),dr.GetDecimal(2),dr.GetSqlMoney(3),dr.GetSqlMoney(4),dr.GetInt32(5),dr.GetDateTime(6),dr.GetString(7),dr.GetString(8),dr.GetString(9));
                    
                    dato = new ventas_local();
                    dato.id = dr.GetInt32(0);
                    dato.prod = dr.GetString(1);
                    dato.cantidad = dr.GetDecimal(2);
                    dato.precio = dr.GetDecimal(3);
                    dato.costo = dr.GetDecimal(4);
                    dato.venta = dr.GetInt32(5);
                    dato.ts = dr.GetDateTime(6);
                    dato.linea = dr.GetString(7);
                    dato.caja = dr.GetString(8);
                    dato.cajero = dr.GetString(9);
                    vl.Add(dato);
                    Console.WriteLine("Dato extraido correctamente");
                    //this.cerrarConexion();
                    n++;
                    if (n == 100)
                    {
                        n = 0;
                        this.insertar();
                        dr3 = dr;
                        dr.Close();
                        this.insertarIdRemoto();
                        dr = dr3;
                        vl = new List<ventas_local>();
                    }
                }
            }
            dr.Close();
            this.cerrarConexion(cnLocal);
            Console.ReadLine();
        }

        public void insertar()
        {
            //vr = new List<ventas_remoto>();
            //ventas_remoto dato = new ventas_remoto(); ;
            this.connectRemoto();
            int n = 0;
            cmd = new SqlCommand(insertarRemoto, cnRemoto);
            
            foreach(ventas_local l in vl)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("prod", l.prod);
                cmd.Parameters.AddWithValue("cantidad", l.cantidad);
                cmd.Parameters.AddWithValue("precio", l.precio);
                cmd.Parameters.AddWithValue("costo", l.costo);
                cmd.Parameters.AddWithValue("venta", l.venta);
                cmd.Parameters.AddWithValue("ts", l.ts);
                cmd.Parameters.AddWithValue("linea", l.linea);
                cmd.Parameters.AddWithValue("caja", l.caja);
                cmd.Parameters.AddWithValue("cajero", l.cajero);
                cmd.Parameters.AddWithValue("idoriginal", l.id);
                cmd.Parameters.AddWithValue("Original", dbOringen);
                n = cmd.ExecuteNonQuery();
                
                cmd2 = new SqlCommand(identity, cnRemoto);
                dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        //Console.WriteLine(dr.GetInt32(1));
                        l.idRemoto = Convert.ToInt32(dr2.GetDecimal(0));
                    }
                }
                dr2.Close();
                xRemoto = l.idRemoto;
                xLocal = l.id;
                Console.WriteLine("Se inserto " + l.prod + " con precio " + l.precio + " IdRemoto" + l.idRemoto);
            }
            //Poner el idRemoto en la base de datos local


            this.cerrarConexion(cnRemoto);
        }

        private void insertarIdRemoto()
        {
            this.connectRemoto();
            int n = 0;
            //queryInsertarIdRemoto = queryInsertarIdRemoto + " where id = " + xLocal;
            cmd3 = new SqlCommand(queryInsertarIdRemoto, cnLocal);
            //Guardar una variable
            
            foreach (ventas_local l in vl)
            {
                Console.WriteLine("IdRemoto: " + l.idRemoto + " IdOriginal " + l.id);
                cmd3.Parameters.Clear();
                cmd3.Parameters.AddWithValue("idRemoto", l.idRemoto);
                cmd3.Parameters.AddWithValue("id", l.id);
                n = cmd3.ExecuteNonQuery();
                Console.WriteLine("Se inserto el idRemoto: " + l.idRemoto);
            }
        }
    }
}
