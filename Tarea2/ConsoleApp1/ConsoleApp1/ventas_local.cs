﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class ventas_local
    {
        public int id { get; set; }
        public String prod { get; set; }
        public Decimal cantidad { get; set; }
        public Decimal precio { get; set; }
        public Decimal costo { get; set; }
        public int venta { get; set; }
        public DateTime ts { get; set; }
        public String linea { get; set; }
        public String caja { get; set; }
        public String cajero { get; set; }
        public int idRemoto { get; set; }
    }
}
