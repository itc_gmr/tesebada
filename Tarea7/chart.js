
var articulosSegundoData = [];
var articulosMinutoData = [];
var importeSegundoData = [];
var importeMinutoData = [];

$( document ).ready(function() {
    
    
/*
    //Leer y cargar el chart de Artículos
    readFile("Articulo_Segundo.txt", articulosSegundoData);
    readFile("Articulo_Minuto.txt", articulosMinutoData);
    readFile("Importe_Segundo.txt", importeSegundoData);
    readFile("Importe_Minuto.txt", importeMinutoData);
    
    //Chart
    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(drawChart);
*/

});

function getDataFromDB() {
    var rfc = document.getElementById("rfc").value;
    if(!rfc.match(/\S/)) {
      alert("Ingresa un RFC");
      return;
    }

    $.ajax({
        url:"test.php/?rfc="+rfc,
        dataType:'json',
        type:'get',
        success:function(result){
            //Convertir los segundos/minutos de number a string
            for(var i=0; i<result.length; i++){//Todo el arreglo
              for(var j=0; j<result[i].length; j++){//Arreglo tipo de datos
                result[i][j][0] = result[i][j][0].toString();
                result[i][j][1] = Number(result[i][j][1]);
              }
            }


            //Ya tenemos los datos de manera correcta, es momento de generar los arreglos de datos.
            articulosSegundoData = result[0];
            articulosMinutoData = result[1];
            importeSegundoData = result[2];
            importeMinutoData = result[3];

            console.log(articulosSegundoData);
            console.log(articulosMinutoData);
            console.log(importeSegundoData);
            console.log(importeMinutoData);

            //-----En este punto ya tenemos los 4 arreglos para las 4 gráficas.


            //Chart
            google.charts.load('current', {packages: ['corechart']});
            google.charts.setOnLoadCallback(drawChart); 

        }

    }); 
}

/*
function readFile(file, array) {
    $.get(file, function(d){
        d = d.split(/\n/);
        for(var i = 0; i < d.length; i++){
            array.push(d[i].split("\t"));

            //String to number
            array[i][0] = Number(array[i][0]);

            //Flip
            var aux = array[i][0];
            array[i][0] = array[i][1];
            array[i][1] = aux;

        }
        console.log("Read file");
        console.log(array);

    });
 
}

*/

function drawChart() {

//Primer chart (Artículo segundo)

   // Define the chart to be drawn.
   var data = new google.visualization.DataTable();
   data.addColumn('string', 'Segundos');
   data.addColumn('number', 'Artículos');
   data.addRows(articulosSegundoData);
   
   // Set chart options
   var options = {'title':'Total de artículo cliente en segundos',
      'width':550,
      'height':400,
       vAxis: { title: "Artículos" },
       hAxis: { title: "Segundos" }

  };

   // Instantiate and draw the chart.
   var chart = new google.visualization.ColumnChart(document.getElementById('container'));
   chart.draw(data, options);




//Segundo chart (Artículo minuto)

   // Define the chart to be drawn.
   data = new google.visualization.DataTable();
   data.addColumn('string', 'Minutos');
   data.addColumn('number', 'Artículos');
   data.addRows(articulosMinutoData);
   
   // Set chart options
   options = {'title':'Total de artículos cliente en minutos',
      'width':550,
      'height':400,
       vAxis: { title: "Artículos" },
       hAxis: { title: "Minutos" }

  };

   // Instantiate and draw the chart.
   chart = new google.visualization.ColumnChart(document.getElementById('container2'));
   chart.draw(data, options);

//Tercer chart (Importe segundo)

   // Define the chart to be drawn.
   data = new google.visualization.DataTable();
   data.addColumn('string', 'Segundos');
   data.addColumn('number', 'Importe');
   data.addRows(importeSegundoData);
   
   // Set chart options
   options = {'title':'Total de importe cliente en segundos',
      'width':550,
      'height':400,
       vAxis: { title: "Importe" },
       hAxis: { title: "Segundos" }

  };

   // Instantiate and draw the chart.
   chart = new google.visualization.ColumnChart(document.getElementById('container3'));
   chart.draw(data, options);


//Cuarto chart (Importe minuto)

   // Define the chart to be drawn.
   data = new google.visualization.DataTable();
   data.addColumn('string', 'Minutos');
   data.addColumn('number', 'Importe');
   data.addRows(importeMinutoData);
   
   // Set chart options
   options = {'title':'Total de importe cliente en minutos',
      'width':550,
      'height':400,
       vAxis: { title: "Importe" },
       hAxis: { title: "Minutos" }

  };

   // Instantiate and draw the chart.
   chart = new google.visualization.ColumnChart(document.getElementById('container4'));
   chart.draw(data, options);
}

