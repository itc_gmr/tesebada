﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Tarea7
{
    class Program
    {
        static List<Clientes> ListaClientes;
        static string dbLectura = "Data Source=::1,1433;Initial Catalog=ventas;Integrated Security=True";
        static string dbAlmacen = "Data Source=::1,1433;Initial Catalog=AlmacenDatos;Integrated Security=True";
        static void Main(string[] args)
        {
            new Program();
        }
        public Program()
        {
//            ListaClientes = new List<Clientes>();
            ListaClientes = LeerSql().Result;
            LeerXml(ListaClientes);
            LeerTexto(ListaClientes);
            EscribirAlmacen();
            foreach (var c in ListaClientes)
            {
                Console.WriteLine($"Fecha: {c.Fecha} RFC: {c.Cliente} Nombre: {c.NombreCliente} Tipo: {c.ClaseDeCliente}");
                foreach(var a in c.Articulos)
                {
                    Console.WriteLine($"Codigo: {a.Codigo} Linea: {a.Linea} Descripcion: {a.Descripcion} Cantidad: {a.Cantidad} Precio: {a.Precio}");
                }
            }
            Console.ReadLine();
        }

        public void LeerTexto(List<Clientes> ListaClientes)
        {
            var carpetas = Directory.GetDirectories(@"E:\TESEBADA\Tarea7\Debug\datos");
            string[] files = null;
            Clientes cliente =null;
            Articulos articulo = null;
            foreach (var c in carpetas)
            {
                files = Directory.GetFiles(@"" + c);
            
                Regex fecha = new Regex(@"^[\d]{4}[-][a-zA-Z]{3}[.][-][\d]{2}[\s][\d]{2}[:][\d]{2}[:][\d]{2}$");
                Regex persona = new Regex(@"(Persona|Compañia)\s[:]\s");
                Regex linea = new Regex(@"(LINEA)\s[a-zA-Z]+");
                Regex partidas = new Regex(@"^[\d]+[x].+[$]$");
                Hashtable meses = new Hashtable();
                meses.Add("ene.", 01);
                meses.Add("feb.", 02);
                meses.Add("mar.", 03);
                meses.Add("abr.", 04);
                meses.Add("may.", 05);
                meses.Add("jun.", 06);
                meses.Add("jul.", 07);
                meses.Add("ago.", 08);
                meses.Add("sep.", 09);
                meses.Add("oct.", 10);
                meses.Add("nov.", 11);
                meses.Add("dic.", 12);
                string cadLinea = "";
                string stLinea="";
                foreach (string txt in files)
                {
                    List<string> lines = File.ReadAllLines(txt).ToList<String>();
                    cliente = new Clientes();
                    for (int i = 0; i < lines.Count; i++)
                    {
                        if(fecha.IsMatch(lines.ElementAt(i)))
                        {
                            var date = lines.ElementAt(i).Split(new char[] {'-', ' '});
                            cliente.Fecha = DateTime.Parse(date[0] + "-" + meses[date[1]].ToString() + "-" + date[2] + " " + date[date.Length - 1] + ".000");
                            continue;
                        }
                        if(persona.IsMatch(lines.ElementAt(i)))
                        {
                            var nombre_clave = lines.ElementAt(i).Split(new char[] { ':', '(', ')' });
                            cliente.ClaseDeCliente = nombre_clave[0];
                            cliente.NombreCliente = nombre_clave[1].Substring(1);
                            cliente.Cliente = nombre_clave[nombre_clave.Length-2];
                            continue;
                        }
                        if(linea.IsMatch(lines.ElementAt(i))) 
                        {
                            var linea_sola = lines.ElementAt(i).Split();
                            stLinea = linea_sola[(linea_sola.Length - 1)].Trim();
                            continue;
                        }            
                        if(partidas.IsMatch(lines.ElementAt(i)))
                        {
                            articulo = new Articulos();
                            var part = lines.ElementAt(i).Split(new char[] { 'x', '(', ')', '@', '$' });
                            articulo.Linea = stLinea;
                            articulo.Cantidad = Decimal.Parse(part[0]);
                            articulo.Descripcion = part[1];
                            articulo.Codigo = part[2];
                            articulo.Precio = decimal.Parse(part[part.Length-2]);
                            cliente.Articulos.Add(articulo);
                            continue;
                        }
                    }
                    ListaClientes.Add(cliente);
                }
            }
        }

        public void LeerXml(List<Clientes> ListaClientes)
        {
            var carpetas = Directory.GetDirectories(@"E:\TESEBADA\Tarea7\Debug\xml");
            string[] files = null;
            Clientes cliente = null;
            Articulos articulo = null;
            foreach (var q in carpetas)
            {
                files = Directory.GetFiles(@"" + q);
            
                foreach(var f in files)
                {
                    cliente = new Clientes();
                    XDocument doc = XDocument.Load(f);
                    var fecha = doc.Descendants("Venta").Select(d=> d.Attribute("Fecha").Value).ElementAt(0);
                    var RFC = doc.Descendants("Cliente").Select(d=> d.Attribute("RFC").Value).ElementAt(0);
                    var nombre = doc.Descendants("Cliente").Select(d => d.Attribute("Nombre").Value).ElementAt(0);
                    var tipo = doc.Descendants("Cliente").Select(d => d.Attribute("Tipo").Value).ElementAt(0);
                    cliente.Fecha =DateTime.Parse(fecha);
                    cliente.Cliente = RFC;
                    cliente.NombreCliente = nombre;
                    cliente.ClaseDeCliente = tipo;
                    //Console.WriteLine($"Fecha: {fecha} RFC: {RFC} Nombre: {nombre} Tipo: {tipo}");
                    var codigos = doc.Descendants("Partida").Select(d => d.Attribute("Codigo").Value);
                    var lineas = doc.Descendants("Partida").Select(d => d.Attribute("Linea").Value);
                    var descripciones = doc.Descendants("Partida").Select(d => d.Attribute("Descripcion").Value);
                    var precios = doc.Descendants("Partida").Select(d => d.Attribute("Precio").Value);
                    var cantidades = doc.Descendants("Partida").Select(d=> d.Attribute("Cantidad").Value);
                    int i = 0;
                    foreach (var c in codigos)
                    {
                        articulo = new Articulos();
                        articulo.Codigo = c;
                        articulo.Linea = lineas.ElementAt(i);
                        articulo.Descripcion = descripciones.ElementAt(i);
                        articulo.Precio = decimal.Parse(precios.ElementAt(i));
                        articulo.Cantidad = decimal.Parse(cantidades.ElementAt(i));
                        cliente.Articulos.Add(articulo);
                    }
                    ListaClientes.Add(cliente);
                }
            }
        }

        static string LeerDatos = "Select v.ID, V.Cliente, V.NombreCliente, V.ClaseDeCliente, V.Fecha, P.Codigo, P.Linea, P.Descripcion, P.Precio, P.Cantidad From Ventas V, PartidasVentas P where V.ID = P.VentaID";

        public static async Task<List<Clientes>> LeerSql()
        {
            var cmd = new SqlCommand(LeerDatos);
            Clientes cliente = null;
            Articulos articulo = null;
            ListaClientes = new List<Clientes>();
            var Ventas = new Dictionary<long, Clientes>();
            try
            {
                using (var con = new SqlConnection(dbLectura))
                {
                    
                    cmd.Connection = con;
                    await cmd.Connection.OpenAsync();
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        
                        long Id;
                        while (await reader.ReadAsync())
                        {
                            cliente = new Clientes();
                            articulo = new Articulos();
                            Id = reader.GetInt64(0);
                            cliente.Cliente = reader.GetString(1);
                            cliente.NombreCliente = reader.GetString(2);
                            cliente.ClaseDeCliente = reader.GetString(3);
                            cliente.Fecha = reader.GetDateTime(4);
                            articulo.Codigo = reader.GetString(5);
                            articulo.Linea = reader.GetString(6);
                            articulo.Descripcion = reader.GetString(7);
                            articulo.Precio = reader.GetDecimal(8);
                            articulo.Cantidad = reader.GetDecimal(9);
                            if (!Ventas.ContainsKey(Id))
                            {
                                Ventas.Add(Id,cliente);
                            }
                            if (Ventas.ContainsKey(Id))
                            {
                                Ventas[Id].Articulos.Add(articulo);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"No se pudieron leer las ventas, error {e.Message}");
            }
            return Ventas.Values.ToList();
        }
        static string InsertarCliente = "insert into Cliente ("
                                        + "RFC, "
                                        + "NombreCliente, "
                                        + "ClaseDeCliente)"
                                        + " Output(inserted.ID) Values ("
                                        + "@RFC, "
                                        + "@NombreCliente, "
                                        + "@ClaseDeCliente)";
        static string InsertarArticulos = "insert into Articulos Output(inserted.ID) default values";
        static string InsertarArticulo = "insert into Articulo ("
                                        + "ID_Articulos, "
                                        + "Codigo, "
                                        + "Linea, "
                                        + "Descripcion, "
                                        + "precio, "
                                        + "Cantidad) "
                                        + " Values (" 
                                        + "@ID_Articulos, "
                                        + "@Codigo, "
                                        + "@Linea, "
                                        + "@Descripcion, "
                                        + "@precio, "
                                        + "@Cantidad)";
        static string InsertarTiempo = "insert into Tiempo ("
                                        + "Fecha, "
                                        + "Dia, "
                                        + "Minuto, "
                                        + "Segundo) "
                                        + "Output(inserted.ID) Values ("
                                        + "@Fecha, "
                                        + "@Dia, "
                                        + "@Minuto, "
                                        + "@Segundo)";
        static string InsertarVentas = "insert into Ventas ("
                                        + "IDCliente, "
                                        + "IDArticulo, "
                                        + "IDTiempo, "
                                        + "Total_Articulo_Cliente, "
                                        + "Total_Importe_Cliente) "
                                        /*+ "Total_Articulo_Tipo_Cliente, "
                                        + "Total_Importe_Tipo_Cliente, "
                                        + "Total_Articulo_Linea, "
                                        + "Total_Importe_Linea) "*/
                                        + "Values ("
                                        + "@IDCliente, "
                                        + "@IDArticulo, "
                                        + "@IDTiempo, "
                                        + "@Total_Articulo_Cliente, "
                                        + "@Total_Importe_Cliente) ";
                                        /*+ "@Total_Articulo_Tipo_Cliente, "
                                        + "@Total_Importe_Tipo_Cliente, "
                                        + "@Total_Articulo_Linea, "
                                        + "@Total_Importe_Linea)";*/
        public void EscribirAlmacen()
        {
            foreach (var c in ListaClientes)
            {
                EscribirUnCliente(c);
            }
        }
        public void EscribirUnCliente(Clientes c)
        {
            var cmd = new SqlCommand(InsertarCliente);
            cmd.Parameters.AddWithValue("@RFC",c.Cliente);
            cmd.Parameters.AddWithValue("@NombreCliente", c.NombreCliente);
            cmd.Parameters.AddWithValue("@ClaseDeCliente", c.ClaseDeCliente);
            var cmdTiempo = new SqlCommand(InsertarTiempo);
            cmdTiempo.Parameters.AddWithValue("@Fecha", c.Fecha);
            cmdTiempo.Parameters.AddWithValue("@Dia", c.Fecha.Day);
            cmdTiempo.Parameters.AddWithValue("@Minuto", c.Fecha.Minute);
            cmdTiempo.Parameters.AddWithValue("@Segundo", c.Fecha.Second);
            try
            {
                using (var con = new SqlConnection(dbAlmacen))
                {
                    //Console.WriteLine($"Cliente: {c.Cliente} Nombre: {c.NombreCliente} Clase: {c.ClaseDeCliente} Fecha: {c.Fecha}");
                    int IDCliente = 0, IDTiempo = 0, IDArticulo = 0;
                    //cliente
                    cmd.Connection = con;
                    cmd.Connection.Open();
                    var ret = cmd.ExecuteScalar();
                    if (ret != null && ret is int)
                        IDCliente = (int)ret;
                    //Tiempo
                    cmdTiempo.Connection = con;
                    ret = cmdTiempo.ExecuteScalar();
                    if (ret != null && ret is int)
                        IDTiempo = (int)ret;
                    //Articulos
                    var cmdArticulos = new SqlCommand(InsertarArticulos);
                    cmdArticulos.Connection = con;
                    ret = cmdArticulos.ExecuteScalar();
                    if (ret != null && ret is int)
                        IDArticulo = (int)ret;
                    foreach (var a in c.Articulos)
                    {
                        //Cada articulo
                        
                        var cmdArticulo = new SqlCommand(InsertarArticulo);
                        cmdArticulo.Parameters.AddWithValue("@ID_Articulos", IDArticulo);
                        cmdArticulo.Parameters.AddWithValue("@Codigo", a.Codigo);
                        cmdArticulo.Parameters.AddWithValue("@Linea", a.Linea);
                        cmdArticulo.Parameters.AddWithValue("@Descripcion", a.Descripcion);
                        cmdArticulo.Parameters.AddWithValue("@precio", a.Precio);
                        cmdArticulo.Parameters.AddWithValue("@Cantidad", a.Cantidad);
                        cmdArticulo.Connection = con;
                        cmdArticulo.ExecuteNonQuery();
                        //Console.WriteLine($"Codigo: {a.Codigo} Linea: {a.Linea} Descripción: {a.Descripcion} Precio: {a.Precio}");
                    }
                    //Se agregan al hecho
                    var cmdVentas = new SqlCommand(InsertarVentas);
                    cmdVentas.Parameters.AddWithValue("@IDCliente", IDCliente);
                    cmdVentas.Parameters.AddWithValue("@IDArticulo", IDArticulo);
                    cmdVentas.Parameters.AddWithValue("@IDTiempo", IDTiempo);
                    cmdVentas.Parameters.AddWithValue("@Total_Articulo_Cliente", c.Articulos.Sum(d=>d.Cantidad));
                    cmdVentas.Parameters.AddWithValue("@Total_Importe_Cliente", c.Articulos.Sum(d=>d.Cantidad*d.Precio));
                    //cmdVentas.Parameters.AddWithValue("@Total_Articulo_Linea", );
                    //cmdVentas.Parameters.AddWithValue("@Total_Importe_Linea", );
                    cmdVentas.Connection = con;
                    cmdVentas.ExecuteNonQuery();
                }
            }
            catch(Exception e)
            {
                Console.WriteLine($"No se pudieron insertar las ventas {e.Message}");
            }
            //cmd.Parameters.AddWithValue("", );
            
        }
    }
    public class Clientes {
        public string Cliente { get; set; }
        public string NombreCliente { get; set; } 
        public string ClaseDeCliente { get; set; }
        public DateTime Fecha { get; set; }
        public List<Articulos> Articulos { get; private set; } = new List<Articulos>();
    }

    public class Articulos
    {
        public string Codigo { get; set; }
        public string Linea { get; set; }
        public string Descripcion { get; set; }
        public Decimal Precio { get; set; }
        public Decimal Cantidad { get; set; }
    }
}
