/*Create database AlmacenDatos
Go*/

use AlmacenDatos

create table Cliente(
	ID int Identity(1,1) not null Primary key,
	RFC NVARCHAR(15) not null,
	NombreCliente NVARCHAR(40) not null,
	ClaseDeCliente NVARCHAR(20) not null
)

--insert into Cliente (RFC, NombreCliente,ClaseDeCliente) Output(inserted.ID) Values (@RFC, @NombreCliente, @ClaseDeCliente)

create table Articulos(
	ID int Identity(1,1) not null primary key
)

--insert into Articulos Output(inserted.ID) default values 

create table Articulo(
	ID_Articulos int not null References Articulos(ID),
	Codigo NVARCHAR(30) not null,
	Linea NVARCHAR(30) not null,
	Descripcion NVARCHAR(80) not null,
	precio Decimal,
	Cantidad Decimal
)

--insert into Articulo (ID_Articulos, Codigo, Linea, Descripcion, precio, Cantidad) Output(inserted.ID) Values (@ID_Articulos, @Codigo, @Linea, @Descripcion, @precio, @Cantidad)
create table Tiempo(
	ID int Identity(1,1) not null primary key,
	Fecha DateTime not null,
	Dia int not null,
	Minuto int not null,
	Segundo int not null,
)

--insert into Tiempo (Fecha, Dia, Minuto, Segundo) Output(inserted.ID) Values (@Fecha, @Dia, @Minuto, @Segundo)

create table Ventas(
	IDCliente int not null references Cliente(ID),
	IDArticulo int not null references Articulos(ID),
	IDTiempo int not null references Tiempo(ID),
	Total_Articulo_Cliente int, --Select Sum(a.cantidad) FRom Articulos a, Clientes c  where c.RFC = a.RFC
	Total_Importe_Cliente Decimal,
	Total_Articulo_Tipo_Cliente int,
	Total_Importe_Tipo_Cliente Decimal,
	Total_Articulo_Linea int,
	Total_Importe_Linea Decimal
)

--insert into Ventas (IDCliente, IDArticulo, IDTiempo, Total_Articulo_Cliente, Total_Importe_Cliente, Total_Articulo_Tipo_Cliente, Total_Importe_Tipo_Cliente, Total_Articulo_Linea, Total_Importe_Linea) Values (@IDCliente, @IDArticulo, @IDTiempo, @Total_Articulo_Cliente, @Total_Importe_Cliente, @Total_Articulo_Tipo_Cliente, @Total_Importe_Tipo_Cliente, @Total_Articulo_Linea, @Total_Importe_Linea)

/*drop table Ventas
drop table Cliente
drop table Articulo
drop table Articulos
drop table Tiempo*/

Select * From Cliente
Select * From Ventas
Select * From Articulo
Select * From Articulos
Select * From Tiempo

delete from Ventas
delete from Cliente
delete from Articulo
delete from Articulos
delete from Tiempo

SELECT v.Total_Articulo_Cliente, T.Segundo  From Ventas v, Tiempo T, Cliente C where C.RFC = 'MAY981231DQ4' And V.IDCliente = C.ID And T.ID = V.IDTiempo
SELECT v.Total_Importe_Cliente, T.Segundo  From Ventas v, Tiempo T, Cliente C where C.RFC = 'MAY981231DQ4' And V.IDCliente = C.ID And T.ID = V.IDTiempo
SELECT v.Total_Articulo_Cliente, T.Minuto  From Ventas v, Tiempo T, Cliente C where C.RFC = 'MAY981231DQ4' And V.IDCliente = C.ID And T.ID = V.IDTiempo
SELECT v.Total_Importe_Cliente, T.Minuto  From Ventas v, Tiempo T, Cliente C where C.RFC = 'MAY981231DQ4' And V.IDCliente = C.ID And T.ID = V.IDTiempo