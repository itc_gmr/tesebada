CREATE TABLE dbo.Ventas
(
    id INT NOT NULL,
    Prod NVARCHAR(200) NOT NULL,
    CANTIDAD DECIMAL(10,4) NOT NULL,
    PRECIO MONEY  NOT  NULL,
    COSTO MONEY NOT NULL,
    VENTA INT NOT NULL,
    TS DATETIME NOT NULL,
    LINEA NVARCHAR(50) NOT NULL,
    CAJA NVARCHAR(10) NOT NULL,
CAJERO NVARCHAR(80) NOT NULL

)

BULK INSERT ventas 
   FROM 'C:\Users\Usuario\Desktop\tesebada\TESEBADA.Tarea1.txt'  
   WITH  
     (  
        FIELDTERMINATOR ='\t',  
        ROWTERMINATOR = '\n',  
		FIRSTROW = 2,
		CODEPAGE = 1252
      ); 
      

--5. ¿Cuáles son los 3 artículos mas vendidos y cual es la cantidad?
SELECT TOP 3 prod AS Producto, count(*) AS ProductosVendidos  
FROM Ventas 
GROUP BY prod 
ORDER BY count(*) DESC
--3 productos más vendidos son: 7.50102E+12 - THINER0011 - 7.91279E+11

--6. ¿Cuál es la línea de artículos mas vendida y porque?
SELECT TOP 1 LINEA, SUM(CANTIDAD) AS Cantidad_vendida 
FROM ventas GROUP BY LINEA 
ORDER BY SUM(CANTIDAD) DESC

--7. Realizar un reporte en el que se vea el total vendido por caja y mes del anio
SELECT * 
FROM (
SELECT caja, cantidad, DATEPART(MONTH, ts) AS M
FROM ventas
)S
PIVOT (SUM(cantidad) FOR M in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])) PV

--8. Realizar un listado de cuáles son los 3 artículos más Vendidos de cada mes.
SELECT Mes, [1] AS 'Más vendido',[2] AS 'Segundo más Vendido',[3] AS 'Tercero más Vendido' FROM (
SELECT DATENAME(MONTH,ts) AS Mes, prod, DENSE_RANK() OVER(PARTITION BY DATENAME(MONTH,ts) ORDER BY COUNT(Cantidad) DESC) AS Cantidad 
FROM ventas
GROUP BY DATENAME(MONTH,ts),prod
) S
PIVOT(MAX(prod) FOR Cantidad IN ([1],[2],[3])) P

--9. Mostrar un reporte de los importes vendidos por cajero y una columna para cada mes
SELECT * 
FROM (
SELECT cajero,(cantidad * precio) AS Importe, DATEPART(MONTH, ts) AS M
FROM ventas
)S
PIVOT ( SUM(Importe) FOR M in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])) PV
order by cajero asc

--10. Producir un reporte en el que ponga el promedio de articulos vendidos por bloque, por cuarto de anio
SELECT * 
FROM (
SELECT prod,cantidad, DATEPART(QUARTER, ts) AS Q
FROM ventas
)S
PIVOT (AVG(cantidad) FOR Q in ([1],[2],[3],[4])) PV
ORDER BY prod ASC

--11. Sacar un listado de artículos y meses durante los cuales un artículo se vendió por debajo del 5% de utilidad.
SELECT s.Producto, S.Mes, S.Porcentaje FROM (
SELECT Producto, Mes, ((P.Utilidad / P.Importe) * 100) AS [Porcentaje] FROM(
SELECT prod AS [Producto], DATENAME(MONTH, TS) AS [Mes], (cantidad * precio) AS Importe, ((cantidad * precio) - (cantidad * costo)) AS Utilidad FROM Ventas) P
WHERE P.Importe <> 0) S
WHERE s.Porcentaje < 5
