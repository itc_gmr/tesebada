﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Tarea5
{
    class Program
    {
        public Program()
        {
            Prueba();
            Escribir();
            Leer();
            XmlPorURL();
            XmlCFDI();
        }
        void Prueba()
        {
            var files = Directory.GetFiles("E:\\TESEBADA\\Tarea5\\CFDI", "*.*", SearchOption.AllDirectories);
            foreach (var f in files)
                Console.WriteLine(f);
            Console.ReadLine();
        }
        void XmlPorURL()
        {
            XDocument doc = XDocument.Load("http://itculiacan.edu.mx/feed/");
            var titulos = doc.Descendants("item").Select(d => d.Element("title").Value);
            var links = doc.Descendants("item").Select(d => d.Element("link").Value);
            int i=0;
            foreach(var n in titulos)
            {
                Console.WriteLine("Titulo:  " + n);
                Console.WriteLine("Link:    " + links.ElementAt(i));
                i++;
            }
            Console.ReadLine();
        }
        void XmlCFDI()
        {
            List<String> rutas = new List<String>();
            rutas.Add("E:\\TESEBADA\\Tarea5\\CFDI\\-2017-02-26-CCO8605231N4_FAC_HER3369a946-9b18-4a5d-b08a-b33696527da3_20170226.xml");
            rutas.Add("E:\\TESEBADA\\Tarea5\\CFDI\\6e7f6920-d3ad-4079-a3e3-c2a313f99890%201368.xml");
            rutas.Add("E:\\TESEBADA\\Tarea5\\CFDI\\aff6ccce-fb20-417c-8d90-47d132bc43c0.xml");
            rutas.Add("E:\\TESEBADA\\Tarea5\\CFDI\\CETF800512GF4_Factura_FE5419_20170312.xml");
            rutas.Add("E:\\TESEBADA\\Tarea5\\CFDI\\F0000015199.xml");
            rutas.Add("E:\\TESEBADA\\Tarea5\\CFDI\\FACTURA-40354.xml");
            rutas.Add("E:\\TESEBADA\\Tarea5\\CFDI\\SECFD_20170321_121815.xml");
            rutas.Add("E:\\TESEBADA\\Tarea5\\CFDI\\V519000207.xml");
            foreach (var cadRuta in rutas)
            {
                XDocument doc = XDocument.Load(cadRuta);
                var emisor = doc.Descendants(doc.Root.Name.Namespace + "Emisor").Select(d => d.Attribute("nombre"));
                Console.WriteLine("Emisor:    " + emisor.ElementAt(0));
                var articulos = doc.Descendants(doc.Root.Name.Namespace + "Concepto").Select(d => d.Attribute("descripcion"));
                var importes = doc.Descendants(doc.Root.Name.Namespace + "Concepto").Select(d => d.Attribute("importe"));
                int i = 0;
                var total = 0.0;
                foreach(var arti in articulos) {
                    total += (double)importes.ElementAt(i);
                    Console.WriteLine(arti.ToString() + "                     " + importes.ElementAt(i));
                    i++;
                }
                Console.WriteLine("Importe total:    " + total);
                var impuestos = doc.Descendants(doc.Root.Name.Namespace + "Impuestos").Select(d => d.Attribute("totalImpuestosTrasladados"));
                Console.WriteLine(impuestos.ElementAt(0));
            }
            Console.ReadLine();
        }
        void Escribir()
        {
            Random r = new Random();
            Vehiculo[] vehiculos = new Vehiculo[10];
            string[] modelo = { "Deportivo", "Convertible", "Camioneta", "Camion", "Pick up" };
            string[] nombre = { "Lobo", "Tornado", "Mustang", "Camaro", "Lamborgini", "Tsuru", "BMW" };
            string[] marca = { "Nissan", "Ford", "Volkbagen", "Chevrolet" };
            DateTime[] año = { new DateTime(2015, 1, 1), new DateTime(2013, 1, 1), new DateTime(2011, 1, 1), new DateTime(2017, 1, 1), new DateTime(2018, 1, 1) };
            string[] usuario = { "Manuel", "Luis", "Peñu", "Christobal", "Diego", "Alfredo", "Joel", "Gustavo", "Tejeda" };
            string[] descripcion = { "Juanito tenia un auto", "El auto habla", "El auto se maneja solo", "El auto no me obedece", "El auto imploto", "Era nuevo hasta que lo saque" };
            XmlDocument doc = new XmlDocument();
            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);
            XmlElement objVehiculos = doc.CreateElement(string.Empty, "Vehiculos", string.Empty);
            doc.AppendChild(objVehiculos);
            for (int i = 0; i < vehiculos.Length; i++)
            {
                vehiculos[i] = new Vehiculo();
                vehiculos[i].modelo = modelo[r.Next(modelo.Length)];
                vehiculos[i].nombre = nombre[r.Next(nombre.Length)];
                vehiculos[i].marca = marca[r.Next(marca.Length)];
                vehiculos[i].año = año[r.Next(año.Length)];
                Recomendaciones recomendacion = new Recomendaciones();
                recomendacion.usuario = usuario[r.Next(usuario.Length)];
                recomendacion.descripcion = descripcion[r.Next(descripcion.Length)];
                vehiculos[i].recomendaciones = recomendacion;

                XmlElement objVehiculo = doc.CreateElement(string.Empty, "Vehiculo", string.Empty);
                objVehiculos.AppendChild(objVehiculo);

                XmlElement xmlModelo = doc.CreateElement(string.Empty, "Modelo", string.Empty);
                XmlText txtModelo = doc.CreateTextNode(vehiculos[i].modelo);
                xmlModelo.AppendChild(txtModelo);
                objVehiculo.AppendChild(xmlModelo);

                XmlElement xmlNombre = doc.CreateElement(string.Empty, "Nombre", string.Empty);
                XmlText txtNombre = doc.CreateTextNode(vehiculos[i].nombre);
                xmlNombre.AppendChild(txtNombre);
                objVehiculo.AppendChild(xmlNombre);

                XmlElement xmlMarca = doc.CreateElement(string.Empty, "Marca", string.Empty);
                XmlText txtMarca = doc.CreateTextNode(vehiculos[i].marca);
                xmlMarca.AppendChild(txtMarca);
                objVehiculo.AppendChild(xmlMarca);

                XmlElement xmlAño = doc.CreateElement(string.Empty, "Año", string.Empty);
                XmlText txtAño = doc.CreateTextNode(vehiculos[i].año+"");
                xmlAño.AppendChild(txtAño);
                objVehiculo.AppendChild(xmlAño);

                XmlElement xmlRecomendaciones = doc.CreateElement(string.Empty, "Recomendaciones", string.Empty);
                objVehiculo.AppendChild(xmlRecomendaciones);

                XmlElement xmlUsuario = doc.CreateElement(string.Empty, "Usuario", string.Empty);
                XmlText txtUsuario = doc.CreateTextNode(recomendacion.usuario);
                xmlUsuario.AppendChild(txtUsuario);
                xmlRecomendaciones.AppendChild(xmlUsuario);

                XmlElement xmlDescripcion = doc.CreateElement(string.Empty, "Descripcion", string.Empty);
                XmlText txtUDescripcion = doc.CreateTextNode(recomendacion.descripcion);
                xmlDescripcion.AppendChild(txtUDescripcion);
                xmlRecomendaciones.AppendChild(xmlDescripcion);
                Console.WriteLine(vehiculos[i].modelo + "   " + vehiculos[i].nombre + "   " + recomendacion.usuario + "   " + recomendacion.descripcion);
            }
            doc.Save("E:\\TESEBADA\\Tarea5\\Vehiculos.xml");
            Console.Read();
        }
        void Leer()
        {
            XDocument doc = XDocument.Load("E:\\TESEBADA\\Tarea5\\Vehiculos.xml");
            List<Vehiculo> listaVehiculos = doc.Descendants("Vehiculo").Select(
                d => new Vehiculo {
                    modelo = d.Element("Modelo").Value,
                    nombre = d.Element("Nombre").Value,
                    marca = d.Element("Marca").Value,
                    año = Convert.ToDateTime(d.Element("Año").Value),
                    
                }).ToList();
            var recomendaciones = doc.Descendants("Recomendaciones").Select(d => new Recomendaciones
            {
                usuario = d.Element("Usuario").Value,
                descripcion = d.Element("Descripcion").Value
            });
            int i = 0;
            Console.WriteLine("Escribiendo");
            foreach (var e in listaVehiculos)
            {
                e.recomendaciones = recomendaciones.ElementAt(i);
                i++;
                Console.WriteLine("Escribiendo elemento");
                //Console.WriteLine(e.modelo + "   " + e.nombre + "   " + e.marca + "   " + e.año);
                Console.WriteLine(e.modelo+ "   " + e.nombre + "   " + e.marca + "   " + e.año + "   " + e.recomendaciones.usuario + "   " + e.recomendaciones.descripcion);
            }
            Console.ReadLine();
        }
        static void Main(string[] args)
        {
            new Program();
        }
    }
    public class Vehiculo
    {
        public string modelo { get; set; }
        public string nombre { get; set; }
        public string marca { get; set; }
        public DateTime año { get; set; }
        public Recomendaciones recomendaciones { get; set; }
    }
    public class Recomendaciones
    {
        public string usuario { get; set; }
        public string descripcion { get; set; }
    }
}
